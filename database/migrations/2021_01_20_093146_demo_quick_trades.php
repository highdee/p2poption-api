<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DemoQuickTrades extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('quick_trading_histories', function (Blueprint $table) {
            $table->id();
            $table->string('ip_address');
            $table->integer('side');
            $table->string('account_type');
            $table->string('order_id');
            $table->string('asset');
            $table->dateTime('open_time');
            $table->dateTime('close_time');
            $table->double('open_price');
            $table->double('close_price');
            $table->double('amount');
            $table->double('profit');
            $table->double('percentage');
            $table->integer('seconds');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
