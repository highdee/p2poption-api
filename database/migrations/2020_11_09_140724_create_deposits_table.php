<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDepositsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //"event":"deposit"
        //id = Test merchant id
        //currency = Currency label
        //amount = Quantity
        //address = address
        //txid = Unique number
        //confirm = Number of approvals
        //state = state

        Schema::create('deposits', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id');
            $table->string('currency')->nullable();
            $table->double('amount')->nullable();
            $table->integer('status')->nullable();
            $table->string('merchant_id')->nullable();
            $table->string('address');
            $table->string('txn_id')->nullable();
            $table->integer('confirm')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('deposits');
    }
}
