<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UserSeeder::class);



       DB::table('admins')->insert([
           'username' => 'admin',
           'password' => Hash::make(123456)
       ]);
//
        DB::table('exchange_rate_extras')->insert([
            'currency' => 'eth',
            'percentage' => 2
        ]);
        DB::table('exchange_rate_extras')->insert([
            'currency' => 'usdt',
            'percentage' => 2
        ]);
        DB::table('exchange_rate_extras')->insert([
            'currency' => 'trx',
            'percentage' => 2
        ]);
       DB::table('bonus')->insert([
           'name' => 'default bonus',
           'percentage' => 10
       ]);
       DB::table('bonus')->insert([
           'name' => '50% bonus',
           'percentage' => 50
       ]);
       DB::table('bonus')->insert([
           'name' => '30% bonus',
           'percentage' => 30
       ]);
       DB::table('bonus')->insert([
           'name' => '20% bonus',
           'percentage' => 20
       ]);

        //        SEEDING ASSET TYPES
       DB::table('asset_types')->insert(['title' => 'Forex', 'historical_api' => 'https://api.binance.com/api/v3/klines', 'socket_address' => 'https://wss.live-rates.com/']);
       DB::table('asset_types')->insert(['title' => 'Crypto', 'historical_api' => 'https://api.binance.com/api/v3/klines', 'socket_address' => 'wss://stream.binance.com:9443/ws/']);

        //        SEEDING TRADING TIME
       DB::table('asset_timings')->insert(['name' => 30]);
       DB::table('asset_timings')->insert(['name' => 59]);
//
       DB::table('deposit_currencies')->insert([
           'title'=>'Ethereum',
           'code'=>'eth',
           'image'=>'https://optionp2p.io/assets/img/finance/icon/ethereum.png',
           'percentage_bonus'=>20
       ]);
       DB::table('deposit_currencies')->insert([
           'title'=>'Tether',
           'code'=>'usdt',
           'image'=>'https://optionp2p.io/assets/img/finance/icon/usdt.png',
           'percentage_bonus'=>40
       ]);
           DB::table('deposit_currencies')->insert([
               'title'=>'Tron',
               'code'=>'trx',
               'image'=>'https://optionp2p.io/assets/img/finance/icon/tron.png',
               'percentage_bonus'=>40
           ]);
           DB::table('trade_config')->insert([
               'name'=>'Affiliate percentage',
               'value'=>'20',
               'created_at'=>now(),
               'updated_at'=>now(),
           ]);
           DB::table('trade_config')->insert([
               'name'=>'Deposit bonus',
               'value'=>'10.2',
               'created_at'=>now(),
               'updated_at'=>now(),
           ]);
    }
}
