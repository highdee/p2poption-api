<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AssetType extends Model
{
    //
    public $appends=['assets'];

    public function getAssetsAttribute(){
        return $this->assets()->get();
    }

    public function assets(){
        return $this->hasMany('App\assets','type_id','id');
    }
}
