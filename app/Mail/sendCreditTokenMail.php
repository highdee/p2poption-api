<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class sendCreditTokenMail extends Mailable
{
    use Queueable, SerializesModels;

    public $data=[];
    public function __construct($data)
    {
        $this->data=$data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from(['address' => env('MAIL_USERNAME'), 'name' => 'Credit verification token'])->view('mails.user_credit_token')->subject('Credit user account token')->with($this->data);
    }
}
