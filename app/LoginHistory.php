<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LoginHistory extends Model
{
    //

    protected  $casts=[
        'created_at' => "datetime:Y-m-d H:i:s"
    ];
}
