<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AssetTiming extends Model
{
    public function timing(){
        return $this->belongsToMany('App\AssetTiming','asset_percentage_timing','asset_id','asset_timing_id')->withPivot(['percentage']);
    }
}
