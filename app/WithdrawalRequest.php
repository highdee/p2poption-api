<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WithdrawalRequest extends Model
{
    protected $casts=[
        'created_at' => 'datetime: d, M-Y H:i A',
    ];
    protected $appends=['user'];

    public function getUserAttribute(){
        return $this->user()->first();
    }

    public function user(){
        return $this->belongsTo('App\User', 'user_id', 'id');
    }
}
