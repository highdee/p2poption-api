<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class assets extends Model
{
    protected $appends=['timing','name'];

    public function getNameAttribute(){
        return $this->base_asset.$this->quote_asset;
    }

    public function getTimingAttribute(){
        return $this->timing()->get();
    }

    public function timing(){
        return $this->belongsToMany('App\AssetTiming','asset_percentage_timing','asset_id','asset_timing_id')->withPivot(['percentage']);
    }
}
