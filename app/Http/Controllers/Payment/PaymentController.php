<?php

namespace App\Http\Controllers\Payment;

use App\Deposit;
use App\ExchangeRate;
use App\ExchangeRateExtra;
use App\Http\Controllers\Controller;
use App\ServerError;
use App\User;
use App\Wallet;
use App\WithdrawalRequest;
use Illuminate\Support\Facades\Auth;
use SimpleSoftwareIO\QrCode\Facades\QrCode;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Tymon\JWTAuth\JWTAuth;

class PaymentController extends Controller
{
    //

    public function receivePaymentInfo(Request $request){
        $data=$request->input();

        try{
            $deposit=Deposit::where(['address'=>$data['address']])->first();
            if(!$deposit){
                $deposit=new Deposit();
                $deposit->user_id=0;
                $deposit->address=$data['address'];
            }

            $deposit->currency=$data['currency'];
            $deposit->amount=$data['amount'];
            $deposit->status=$data['state'];
            $deposit->txn_id=$data['txid'];
            $deposit->confirm=$data['confirm'];
            $deposit->merchant_id=$data['id'];
            $deposit->save();

            if($deposit->user_id > 0 && $deposit->status){
                $wallet=Wallet::where(['user_id'=>$deposit->user_id])->first();

                if(!$wallet){
                    $wallet=new Wallet();
                    $wallet->user_id=$deposit->user_id;
                    $wallet->amount=0;
                }

                $bonus = 0;
                //calculate bonus
                $wallet->amount+= ($data['amount'] + $bonus);
                $wallet->save();
            }
        }
        catch (\Exception $e){
            $serverError=new ServerError();
            $data=[
                'location'=>'App\Http\Controllers\Payment::receivePaymentInfo',
                'error'=>json_encode($e->getMessage()),
                'data'=>json_encode($data)
            ];
            $serverError->content=json_encode($data);
            $serverError->save();
        }


        return response([
            'status'=>true,
            'message'=>"Payment info was received successfully!"
        ],200);

    }

    public function getAddress(Request $request){
        $user=Auth::user();
        $details=$request->input();

        $token = $details['currency'] == 'trx' ? env('COINPAY_TOKEN') : env('WIKIPAY_TOKEN');
        $id = $details['currency'] == 'trx' ? env('COINPAY_ID') : env('WIKIPAY_ID');
        $endpoint = $details['currency'] == 'trx' ? env('COINPAY_ENDPOINT') : env('WIKIPAY_ENDPOINT');


        $payload=[
            'token'=> $token,
            'id'=> $id
        ];


        $response=Http::post($endpoint.'wallet/'.$details['currency'].'/add',$payload);

        if($response->status() != 200){
            return response([
                'status'=>false,
                'data'=>$response->body(),
                'msg'=>$response->serverError(),
            ],$response->status());
        }



        $data=json_decode($response->body());

        if($data->result == 'failed'){
            return response([
                'status'=>false,
                'msg'=>"Unable to get payment address"
            ],400);
        }

//        UPDATING WIKIPAY AND COINPAY PAYMENT CALLBACK AS REQUIRED BY THEM
//        THIS IS MEANT TO BE DONE ONES BUT I AM DOING IT OVER AND OVER WHEN A PAYMENT IS INITIALISED (Just for the sake of assurance)
        $callBackpayload=[
            'token'=> $token,
            'callback'=> env('APP_URL').'/api/v1/payment/receive-payment-info'
        ];
        $updateCallbackResponse=Http::post($endpoint.'callback/'.$details['currency'].'/add',$callBackpayload);
//        error_log($updateCallbackResponse->body());

        $filename=mt_rand(1000,999999).'.svg';
        $path=storage_path('qrcodes/'.$filename);
        QrCode::size(180)->generate($data->address, $path);

        $exchangeExtra = ExchangeRateExtra::where('currency', $details['currency'])->first();
        if(!$exchangeExtra){
            return response([
                'status'=>false,
                'msg'=>"Currency is not recognised",
            ],200);
        }


        $priceResponse = $this->convertCurrency($details['currency']);

        if(!$priceResponse){
            return  response([
                'status'=>false,
                'msg'=>"Unable to convert coin rate",
            ],200);
        }

        if($priceResponse > 1) $priceResponse = (float)(1/$priceResponse);

        $percentage = ( $exchangeExtra->percentage / 100 ) * $details['amount'];
        $price = ($percentage + $details['amount']) * ($priceResponse);


        $result=[
            'address'=>$data->address,
            'amount'=>$price,
            'currency'=>$details['currency'],
            'qr'=>'qrcodes/'.$filename
        ];


        $deposit=new Deposit();
        $deposit->user_id=$user->id;
        $deposit->address=$data->address;
        $deposit->save();

        
        return response([
            'status'=>true,
            'data'=>$result
        ],200);
    }

    public function convertCurrency($currency){
        $value=0;
        if($currency == 'usdt') return 1;

        $currency = strtoupper($currency);
        $response=Http::get("https://api.binance.com/api/v3/avgPrice?symbol=".$currency.'USDT');

        if($response->status() != 200) return false;

        $body = json_decode($response->body());
        $value = $body->price;

        return $value;
    }

    public function convertUSDToUSDT($usd){
        $value=0;
        $eth_value=ExchangeRate::where('currency','usdt')->first();
        if($eth_value){
            $value=$eth_value->one_usd_in_currency * $usd;
        }
        return $value;
    }

    public function addWalletAddress(Request $request){
        $user = Auth::user();
        $this->validate($request, [
            'walletAddress' => 'string'
        ]);
        $details=$request->input();

        $addressColumn = Wallet::where('user_id', $user->id)->first();
        $addressColumn->address = $details['walletAddress'];

        $addressColumn->save();

        $user = User::find($user->id);

        return response([
            'status' => true,
            'data' => $user
        ],200);
    }

    public function makeWithdrawalRequest(Request $request){
        $user = Auth::user();
        $details = $request->input();

        $w_req = new WithdrawalRequest();

        $w_req->user_id = $user->id;
        $w_req->amount = $details['amount'];
        $w_req->address = $user->wallet->address;
        $w_req->reference = mt_rand(99999,99999999);

        $w_req->save();

        $user = User::find($user->id);

        return response([
            'status' => true,
            'data' =>$user
        ]);
    }

    public function getHistory(){
        $user = Auth::user();

        $history = WithdrawalRequest::where('user_id', $user->id)->get();

        return response([
            'status' => true,
            'data' => $history
        ],200);
    }

}
