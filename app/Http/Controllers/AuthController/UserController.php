<?php

namespace App\Http\Controllers\AuthController;

use App\AffiliateWallet;
use App\Chat;
use App\DemoWallet;
use App\DepositCurrency;
use App\Http\Controllers\Controller;
use App\LoginHistory;
use App\User;
use App\Verification;
use App\Wallet;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Jobs\verificationMailJob;
use Illuminate\Support\Str;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('jwt.auth');
    }

    public function chargeBalance(Request $request,$account,$trade_amount){
        $user = Auth::user($request->only('token'));
        $amount =$account == 'wallet' ?  $user->wallet->amount : $user->demo_wallet->amount;;

//        VERIFYING ACCOUNT BALANCE
        if($amount < $trade_amount){
            return response(402);
        }

//        CHARGING USER
        switch ($account){
            case 'wallet':
                $wallet = Wallet::where('user_id',$user->id)->first();

                $wallet->amount = $wallet->amount - $trade_amount;
                $wallet->save();
                break;
            default:
                $demo_wallet = DemoWallet::where('user_id',$user->id)->first();
                $demo_wallet->amount = $demo_wallet->amount - $trade_amount;
                $demo_wallet->save();
        }

        $user = User::find($user->id);
        return response($user,200);
    }

    public function updateProfile(Request $request)
    {

        $user = User::where('id', Auth::user($request->token)->id)->first();

        if (isset($request['email'])) {
            $user->email = $request->email;
        }
        if (isset($request['firstname'])) {
            $user->firstname = $request->firstname;
        }
        if (isset($request['lastname'])) {
            $user->lastname = $request->lastname;
        }
        if (isset($request['gender'])) {
            $user->gender = $request->gender;
        }
        if (isset($request['date_of_birth'])) {
            $user->date_of_birth = $request->date_of_birth;
        }
        if (isset($request['phone'])) {
            $user->phone = $request->phone;
        }
        if (isset($request['country'])) {
            $user->country = $request->country;
        }
        if (isset($request['state'])) {
            $user->state = $request->state;
        }
        if (isset($request['city'])) {
            $user->city = $request->city;
        }
        if (isset($request['address'])) {
            $user->address = $request->address;
        }
        if (isset($request['address_two'])) {
            $user->address_two = $request->address_two;
        }
        if (isset($request['zip_code'])) {
            $user->zip_code = $request->zip_code;
        }
        if (isset($request['dial_code'])) {
            $user->dial_code = $request->dial_code;
        }
        if (isset($request['dial_ct'])) {
            $user->dial_ct = $request->dial_ct;
        }

        if (!$user->save()) {
            return response([
                'status' => false,
                'msg' => 'Error updating profile, pls try again',

            ]);
        }

        return response([
            'status' => true,
            'msg' => 'Profile updated successfully',
            'data' => $user
        ]);
    }

    public function uploadProfileImage(Request $request)
    {
        $user = Auth::user($request->only('token'));

        if ($request->hasFile("image")) {
            $file = $request->file('image');
            $fileformats = ['jpg', 'JPG', 'jpeg', 'JPEG', 'png', 'PNG'];

            if (!in_array($file->getClientOriginalExtension(), $fileformats)) {
                return response(400)->json(['msg' => 'file format not Supported']);
            }

            $imagename = date('dmyhis') . $file->getClientOriginalName();
            $imagename = str_replace('(', '', $imagename);
            $imagename = str_replace(')', '', $imagename);

            $file->move(storage_path() . "/profile-images/", $imagename);
            $user->profile_image = "http://" . $request->getHttpHost() . "/api/v1/profile-images/" . $imagename;
            $user->save();
        }

        return response([
            'status' => true,
            'msg' => "Profile Image was updated",
            'data' => $user
        ], 200);
    }

    public function verifyDetails(Request $request)
    {
        $user = Auth::user($request->only('token'));
        $verify = Verification::where('user_id', $user->id)->first();
        if (!$verify) {
            $verify = new Verification();
        }
        $formats = ['jpg', 'JPG', 'jpeg', 'JPEG', 'png', 'PNG'];

        if ($request->hasFile("address_verify")) {
            $file = $request->file('address_verify');


            if (!in_array($file->getClientOriginalExtension(), $formats)) {
                return response(['msg' => 'file format not Supported', 'status' => false], 400);
            }

            $imagename = date('dmyhis') . $file->getClientOriginalName();
            $imagename = str_replace('(', '', $imagename);
            $imagename = str_replace(')', '', $imagename);

            $file->move(storage_path() . "/verification/", $imagename);
            $verify->address_verification = "http://" . $request->getHttpHost() . "/api/v1/verification/" . $imagename;
            $verify->user_id = $user->id;
            $verify->address_status = 0;
            if (!$verify->save()) {
                return response(['msg' => 'Error uploading address verification, pls try again', 'status' => false], 400);
            }

            $user = User::find($user->id);
            return response(['msg' => 'Address verification uploaded successfully', 'status' => true, 'data' => $user], 200);
        }

        if ($request->hasFile("identity_verify")) {
            $file = $request->file('identity_verify');

            if (!in_array($file->getClientOriginalExtension(), $formats)) {
                return response(['msg' => 'file format not Supported', 'status' => false], 400);
            }

            $imagename = date('dmyhis') . $file->getClientOriginalName();
            $imagename = str_replace('(', '', $imagename);
            $imagename = str_replace(')', '', $imagename);

            $file->move(storage_path() . "/verification/", $imagename);
            $verify->identity_verification = "http://" . $request->getHttpHost() . "/api/v1/verification/" . $imagename;
            $verify->user_id = $user->id;
            $verify->identity_status = 0;
            if (!$verify->save()) {
                return response(['msg' => 'Error uploading identity verification, pls try again', 'status' => false], 400);
            }
            $user = User::find($user->id);
            return response(['msg' => 'Identity verification uploaded successfully', 'status' => true, 'data' => $user], 200);
        }

        return response(['msg' => 'No file was uploaded OR file size too large', 'status' => false], 400);
    }

    public function deleteAvatar(Request $request)
    {
        $user = User::where('id', Auth::user($request->query('token'))->id)->first();
        $filename = explode('/', $user->profile_image)[count(explode('/', $user->profile_image)) - 1];
        unlink(storage_path('profile-images/' . $filename));

        $user->profile_image = null;
        if (!$user->save()) {
            return response()->json([
                'status' => false,
                'msg' => 'Error removing avatar, pls try again'
            ]);
        }
        return response()->json([
            'status' => true,
            'msg' => 'Avatar removed successfully',
            'data' => $user
        ]);
    }

    public function get_dashboard(Request $request){
        $user=Auth::user();
        if(!$user){
            return response()->json([
                'status' => false,
                'msg' => 'Token expired, pls login again'
            ]);
        }
        return response([
            'status' => true,
            'data' => array_merge(collect($user)->toArray(), ['ip_address' => $request->getClientIp()]),
        ], 200);
    }

    public function resend_verification(Request $request)
    {
        $user = Auth::user($request->query('token'));
        $user->remember_token = mt_rand(99999, 99999999) . Str::random(12) . mt_rand(99999, 99999999) . Str::random(12);
        $user->save();
        $link = env('APP_FRONTEND') . "/verify-account?code=" . $user->remember_token;
        verificationMailJob::dispatch(['email' => $user->email, 'link' => $link]);
        return response()->json([
            'status' => true,
            'msg' => 'Verification Link Sent Successful Successful'
        ]);
    }

    public function changePassword(Request $request)
    {
        $this->validate($request, [
            'oldPassword' => 'required|string',
            'password' => 'required|confirmed|min:6'
        ]);
        $user = Auth::user($request->query('token'));

        if (!Hash::check($request->oldPassword, $user->password)) {
            return response()->json([
                'status' => false,
                'msg' => 'Incorrect old password'
            ]);
        }
        $user->password = bcrypt($request->password);
        if (!$user->save()) {
            return response()->json([
                'status' => false,
                'msg' => 'Error changing password, pls try again'
            ]);
        }
        return response()->json([
            'status' => true,
            'msg' => 'Password changed sucessfully'
        ]);
    }

    public function sendPhoneToken(Request $request)
    {
        $user = Auth::user($request->query('token'));
        $user->remember_token = mt_rand(1111, 9999);
        $user->save();


        return response([
            'status' => true,
            'msg' => "Verification token has been sent to your phone",
        ]);
    }

    public function sendChat(Request $request)
    {
        $this->validate($request, [
            'receiver' => 'required|integer',
            'message' => 'required|string',
        ]);
        $chat = new Chat();
        $chat->sender = Auth::user($request->query('token'))->id;
        $chat->receiver = $request->receiver;
        $chat->message = $request->message;
        $chat->save();

        $getChats = Chat::where('sender', Auth::user($request->query('token'))->id)
            ->orWhere('receiver', $request->receiver)
            ->orWhere('receiver', Auth::user($request->query('token'))->id)
            ->orderBy('id', 'asc')
            ->get();
        return response([
            'status' => true,
            'chats' => $getChats
        ]);
    }

    public function getSupportChat(Request $request)
    {
        $user = Auth::user($request->query('token'));
        $chats = Chat::where('sender', $user->id)
            ->orWhere('receiver', $request->receiver)
            ->orWhere('receiver', Auth::user($request->query('token'))->id)
            ->orderBy('id', 'asc')
            ->get();

        return response([
            'status' => true,
            'chats' => $chats
        ]);
    }

    public  function creditAffiliateReferralBonus($user_id, $amount, $action)
    {
        $user = User::find($user_id);
        if ($user->referral) {
            $bal = 0;
            $offer = $user->referral[0];
            if ($offer->offer_type == 'Volume sharing' && $action == 1) {
                $bal = ($offer->cashback / 100) * floatval($amount);
            } elseif ($offer->offer_type == 'Turnover sharing' && $action == 2) {
                $bal = ($offer->cashback / 100) * floatval($amount);
            } elseif ($offer->offer_type == 'Deposit sharing' && $action == 3) {
                $bal = ($offer->cashback / 100) * floatval($amount);
            } elseif ($offer->offer_type == 'Revenue sharing' && $action == 4) {
                $bal = ($offer->cashback / 100) * floatval($amount);
            } elseif ($offer->offer_type == 'CPA' && $action == 5) {
                $bal = ($offer->cashback / 100) * floatval($amount);
            }
            $aff = AffiliateWallet::where('affiliate_id', $offer->affiliate_id)->first();
            $aff->amount = floatval($aff->amount) + $bal;
            $aff->save();
        }
    }

    public function loginHistory(Request $request)
    {
        $User = Auth::user();

        $histories = LoginHistory::where('user_id', $User->id)->limit(20)->orderBy('id', 'desc')->paginate(20);

        return $histories;
    }

    public function getDepositCurrency(){
        $data = DepositCurrency::where('status',0)->get();
        return response([
            'status'=>true,
            'data'=>$data
        ]);
    }

    public function verifyCurrencyExist(Request $request){
        $data=DepositCurrency::where('code',$request->code)
                            ->where('status', 0)
                            ->first();
        if(!$data){
            return response([
                'status'=>false,
                'msg'=>'Currency not found'
            ]);
        }
        return response([
            'status'=>true,
            'data'=>$data
        ]);
    }
    public function logout(Request $request)
    {
        $LoginHistories = LoginHistory::where(['user_id' => Auth::user()->id, 'ip_address' => $request->getClientIp(), 'expired_at' => null])->get();
        foreach ($LoginHistories as $history) {
            if ($history) {
                $history->expired_at = date('Y-m-d H:i:s');
                $history->save();
            }
        }
        Auth::logout();
        return response([], 200);
    }
}
