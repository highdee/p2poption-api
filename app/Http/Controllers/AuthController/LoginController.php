<?php

namespace App\Http\Controllers\AuthController;

use App\Affiliate;
use App\Balance;
use App\DemoWallet;
use App\Http\Controllers\Controller;
use App\LoginHistory;
use App\Mail\resetMail;
use App\Referral;
use App\Wallet;
use Illuminate\Http\Request;
use App\Jobs\verificationMailJob;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;
use JWTAuth;
use Auth;
use App\User;

class LoginController extends Controller
{
    public function login(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required'
        ]);


        $credentials = $request->only('email', 'password');
        if ($token = JWTAuth::attempt($credentials)) {

            $user = User::where('id', auth::user($token)->id)->first();
            $this->saveClientLogin($request, $user);

            return response()->json([
                'status' => true,
                'msg' => "Login Successfull",
                'token' => $token,
                "user" => array_merge($user->toArray(), ['ip_address' => $request->getClientIp()])
            ]);
        } else {

            if (isset($request['gmail_login'])) {
                $user = User::where([
                    'email' => $request['email'],
                    'gmail_id' => $request['gmail_id'],
                ])->first();
                if ($user) {
                    $token = JWTAuth::fromUser($user);
                    $this->saveClientLogin($request, $user);

                    return response()->json([
                        'status' => true,
                        'msg' => "Login Successful",
                        'token' => $token,
                        "user" => array_merge(collect($user)->toArray(), ['ip_address' => $request->getClientIp()])
                    ]);
                } else {
                    return response()->json(
                        [
                            "status" => false,
                            "msg" => "Invalid login details please signup"
                        ]
                    );
                }
            } else {
                return response()->json(
                    [
                        "status" => false,
                        "msg" => "Invalid login details please signup"
                    ]
                );
            }
        }
    }

    public function saveClientLogin(Request $request, $user)
    {
        $LoginInfo = new LoginHistory();
        $LoginInfo->user_id = $user->id;
        $LoginInfo->ip_address = $request->getClientIp();
        $LoginInfo->device = $request->device;
        $LoginInfo->browser = $request->browser;
        $LoginInfo->country = 'unknown';
        $LoginInfo->city = 'unknown';
        //        $LoginInfo->token='';
        $LoginInfo->save();
    }

    public function createAccount(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email|unique:users',
            'password' => 'required|confirmed|min:6',
        ]);

        $user = new User();
        $user->email = $request['email'];
        $user->unique_id = substr(sha1(time()), 0, 6);
        $user->password = bcrypt($request['password']);
        $user->remember_token = mt_rand(99999, 99999999) . Str::random(12) . mt_rand(99999, 99999999) . Str::random(12);

        //USER USED GMAIL SIGNUP
        if (isset($request['gmail_login'])) {
            $user->gmail_id = $request['gmail_id'];
        }

        if (isset($request['lastname'])) {
            $user->lastname = $request['lastname'];
            $user->firstname = $request['firstname'];
            $user->profile_image = $request['profile_image'];
            $user->email_verified_at = date('Y-m-d h:i:s');
        } else {
            $link = env('APP_FRONTEND') . "/verify-account?code=" . $user->remember_token;
            //            verificationMailJob::dispatchAfterResponse(['email'=>$user->email,'link'=>$link]);
        }

        $user->save();

        $wal = new Wallet();
        $wal->user_id = $user->id;
        $wal->amount = 0;
        $wal->status = 0;
        $wal->save();

        $demo = new DemoWallet();
        $demo->user_id = $user->id;
        $demo->amount = 1000;
        $demo->save();

        $code = $request->query('referral_id');
        $referral = Referral::where('token', $code)->first();
        if ($referral) {
            DB::table('affiliate_referral_user')->insert([
                'user_id' => $user->id,
                'referral_id' => $referral->id,
                'affiliate_id' => $referral->affiliate->id
            ]);
        }
        $credentials = $request->only('email', 'password');
        if ($token = JWTAuth::attempt($credentials)) {
            $user = User::where('id', auth::user($token)->id)->first();
            return response()->json([
                'status' => true,
                'msg' => "Registration Successful",
                'token' => $token,
                "user" => array_merge(collect($user)->toArray(), ['ip_address' => $request->getClientIp()])
            ]);
        } else {
            return response()->json(
                [
                    "status" => false,
                    "message" => "Invalid login details"
                ]
            );
        }
    }

    public function verify_account(Request $request)
    {
        $token = $request->query('code');

        if (!isset($token)) {
            return response()->json([
                'status' => false,
                'msg' => "Token not found"
            ]);
        }


        $User = User::where('remember_token', $token)->first();

        if (!$User) {
            return response()->json([
                'status' => false,
                'msg' => "Account not found"
            ]);
        }

        $User->email_verified_at = date('Y-m-d h:i:s');
        $User->remember_token = mt_rand(99999, 99999999) . Str::random(12) . mt_rand(99999, 99999999) . Str::random(12);
        $User->save();

        return response()->json([
            'status' => true,
            'msg' => "Account verified, pls login"
        ]);
    }

    public function resend_verification(Request $request, $id)
    {
        $user = User::where('unique_id', $id)->first();
        $user->remember_token = mt_rand(99999, 99999999) . Str::random(12) . mt_rand(99999, 99999999) . Str::random(12);
        $user->save();
        $link = env('APP_URL_FRONTEND') . "/verify-account?code=" . $user->remember_token;
        verificationMailJob::dispatch(['email' => $user->email, 'link' => $link]);
        return response()->json([
            'status' => true,
            'msg' => 'Verification Link Sent Successful Successful'
        ]);
    }

    public function forget_password(Request $request)
    {
        $this->validate($request, [
            'email' => 'required',
        ]);

        $user = User::where('email', $request->input('email'))->first();

        if (!$user) {
            return response()->json([
                'status' => false,
                'msg' => "Account not found"
            ]);
        }
        $token = '';
        $is_mobile = false;

        if(!key_exists('type', $request->input())){
            $token = mt_rand(99999, 99999999) . Str::random(12) . mt_rand(99999, 99999999) . Str::random(12);
            $user->remember_token = $token;
            $user->save();
            $link = env('APP_FRONTEND') . "/reset-password/" . $user->remember_token;
        }else{
            $token = mt_rand(1000,99999);
            $user->remember_token = $token;
            $user->save();
            $link = $token;
            $is_mobile = true;
        }


        Mail::to($user->email)->sendNow(new resetMail([
            'link' => $link,
            'is_mobile' => $is_mobile
        ]));

        return response()->json([
            'status' => true,
            'msg' => "Reset mail has been sent"
        ]);
    }

    public function verify_password(Request $request)
    {
        $token = $request->query('code');

        if (!isset($token)) {
            return response()->json(['status' => false, 'msg' => "Token not found"]);
        }

        $user = User::where('remember_token', $token)->first();

        if (!$user) {
            return response()->json([
                'status' => false,
                'msg' => "Invalid reset password token"
            ]);
        }

        return response()->json(['status' => true, 'msg' => "correct token"]);
    }

    public function reset_password(Request $request)
    {
        $this->validate($request, [
            'password' => 'required|confirmed',
            'token' => 'required'
        ]);

        $user = User::where('remember_token', $request->input('token'))->first();

        if (!$user) {
            return response()->json([
                'status' => false,
                'msg' => "Account not found"
            ]);
        }

        $user->password = bcrypt($request->input('password'));
        $user->remember_token = mt_rand(99999, 99999999) . Str::random(12) . mt_rand(99999, 99999999) . Str::random(12);
        $user->save();

        return response()->json(['status' => true, 'msg' => "Password reset was successful"]);
    }
}
