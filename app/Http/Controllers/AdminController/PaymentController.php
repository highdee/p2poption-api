<?php

namespace App\Http\Controllers\AdminController;

use App\Balance;
use App\Deposit;
use App\DepositCurrency;
use App\ExchangeRate;
use App\ExchangeRateExtra;
use App\Http\Controllers\Controller;
use App\Mail\sendCreditTokenMail;
use App\User;
use App\Wallet;
use App\WithdrawalRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;

class PaymentController extends Controller
{
    public function __construct(){
        $this->middleware('auth:admin');
    }

    public function exchangeRates(){
        $rate=ExchangeRateExtra::all();
        return view('admin.exchange_rates')->with('rates',$rate);
    }
    public function exchangeRate($id){
        $rate=ExchangeRateExtra::find($id);
        if(!$rate){
            HelpController::flashSession(false, 'Rate not found');
            return redirect('exchange-rates');
        }

        $response=Http::get("https://api.binance.com/api/v3/avgPrice?symbol=".strtoupper($rate->currency).'USDT');

        if($response->status() != 200) {
            HelpController::flashSession(false, 'Rate not found');
            return redirect('exchange-rates');
        }
        $body = json_decode($response->body());
        $price = $body->price;
        if($price > 1) $price = (1/$price);

        return view('admin.exchange_rate')->with(['rate'=> $rate, 'current_price'=> $price]);
    }

    public function updateExchangeRate(Request $request){
        $this->validate($request, [
            'rate_id'=>'required|integer',
            'rate'=>'required|numeric'
        ]);
        $rate=ExchangeRateExtra::find($request->rate_id);
        if(!$rate){
            HelpController::flashSession(false, 'Rate not found');
            return redirect()->back();
        }
        $rate->percentage=$request->rate;
        if(!$rate->save()){
            HelpController::flashSession(false, 'Error updating rate, pls try again');
            return redirect()->back();
        }
        HelpController::flashSession(true, 'Exchange rate extra updated successfully');
        return redirect()->back();
    }

    public function deposits(){
        $deposits=Deposit::where('amount','!=',null)->get();
        return view('admin.deposits')->with('deposits', $deposits);
    }

    public function withdrawal(){
        $withdrawals = WithdrawalRequest::get();
        return view('admin.withdrawal')->with('withdrawals', $withdrawals);
    }

    public function handleWithdrawal(Request $request, $action){
       $withdrawal = WithdrawalRequest::where('id', $request->get('id'))->first();

       if ($action == 1){
           $withdrawal->accepted_at = date('Y-m-d H:i:s');
       }else{
           $withdrawal->accepted_at = Null;
           $withdrawal->rejected_at = date('Y-m-d H:i:s');
       }

       $withdrawal->save();
       return redirect()->back();
    }

    public function creditUserAccount(Request $request){
        $this->validate($request, [
            'user_id'=>'required|integer',
            'amount'=>'required|regex:/^\d*(\.\d{2})?$/',
        ]);
        $user=User::find($request->user_id);
        if(!$user){
            HelpController::flashSession(false, 'User not found');
            return redirect('/admin/users');
        }

        $token=Str::random(12);
        $data=[
            'user'=>$user->firstname.' '.$user->lastname,
            'amount'=>$request->amount,
            'token'=>$token
        ];

        Mail::to(env('MAIL_USERNAME'))->send(new sendCreditTokenMail($data));

        DB::table('credit_user_token')->insert([
            'user_id'=>$request->user_id,
            'amount'=>$request->amount,
            'token'=>$token,
        ]);

        HelpController::flashSession(true, 'An OTP has been sent to the admin mail, please enter the received verification token');
        return view('admin.verify_credit_token')->with([
            'amount'=>$request->amount,
            'user'=>$user->firstname.' '.$user->lastname,
            'user_id'=>$request->user_id,
        ]);
    }

    public function verifyCreditToken(Request $request){
        $this->validate($request, [
            'token'=>'required|string'
        ]);
        $verify=DB::table('credit_user_token')->where('token', $request->token)->first();
        if(!$verify){
            HelpController::flashSession(false, 'Incorrect token, pls try again');
            return redirect()->back();
        }

        DB::table('credit_user_token')
            ->where('token', $request->token)
            ->update([
                'token' => Str::random(12),
                'status'=>1
            ]);

        $wallet=Wallet::where('user_id', $verify->user_id)->first();
        $wallet->amount=floatval($wallet->amount) + floatval($verify->amount);
        $wallet->status=1;
        if(!$wallet->save()){
            HelpController::flashSession(false, 'Error crediting account, pls try again');
            return redirect()->back();
        }
        HelpController::flashSession(true, 'User credited successfully');
        return  redirect('/admin/user/'.$verify->user_id);
    }

    public function currencies(){
        $curr = DepositCurrency::all();
        return view('admin.deposit_currencies')->with('currency', $curr);
    }

    public function currency($id){
        $curr = DepositCurrency::find($id);
        if(!$curr){
            HelpController::flashSession(false, 'Deposit currency not found');
            return back();
        }

        return view('admin.deposit_currency')->with('currency', $curr);
    }

    public function updateDepositcurrency(Request $request){
        $this->validate($request, [
           'currency_id'=>'required|integer',
           'bonus_percentage'=>'required|integer'
        ]);

        $curr = DepositCurrency::find($request->currency_id);
        if(!$curr){
            HelpController::flashSession(false, 'Deposit currency not found');
            return back();
        }
        $curr->percentage_bonus = $request->bonus_percentage;

        if($request->status){
            $curr->status = 0;
        }else{
            $curr->status = -1;
        }
        if(!$curr->save()){
            HelpController::flashSession(false, 'Error updating bonus percentage');
            return back();
        }
        HelpController::flashSession(true, 'Bonus percentage updated successfully');
        return back();
    }
}
