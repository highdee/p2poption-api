<?php

namespace App\Http\Controllers\AdminController;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    public function showlogin(){
        return view('admin.login');
    }
    public function login(Request $request){
        $this->validate($request, [
            'username'=>'required|string',
            'password'=>'required'
        ]);

        $credientials=['username'=>$request->username,'password'=>$request->password];
        if(Auth::guard('admin')->attempt($credientials)){
            return redirect()->intended('admin');
        }
        HelpController::flashSession(false,'Incorrect username and password');
        return redirect()->back()->withInput($request->only('username'));
    }
}
