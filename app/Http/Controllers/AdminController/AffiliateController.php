<?php

namespace App\Http\Controllers\AdminController;

use App\Affiliate;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use PHPUnit\TextUI\Help;

class AffiliateController extends Controller
{
    public function __construct(){
        $this->middleware('auth:admin');
    }

    public function index(){
        $affs=Affiliate::all()->sortByDesc('id');
        return view('admin.affiliate.index')->with('affs', $affs);
    }

    public function affiliate($id){
        $aff=Affiliate::find($id);
        if(!$aff){
            HelpController::flashSession(false, 'Affiliate not found');
            return redirect('/admin/affiliates');
        }
        return view('admin.affiliate.affiliate')->with('aff',$aff);
    }

    public function updateAffiliate(Request $request){
        $this->validate($request, [
            'affiliate_id'=>'required|integer',
            'email'=>'required|email'
        ]);
        $aff=Affiliate::find($request->affiliate_id);

        if(isset($request->email)){
            $aff->email=$request->email;
        }
        if(isset($request->username)){
            $aff->username=$request->username;
        }
        if(isset($request->phone)){
            $aff->phone=$request->phone;
        }
        if(isset($request->firstname)){
            $aff->firstname=$request->firstname;
        }
        if(isset($request->lastname)){
            $aff->lastname=$request->lastname;
        }
        if(isset($request->country)){
            $aff->country=$request->country;
        }
        if(!$aff->save()){
            HelpController::flashSession(false, 'Error updating affiliate account');
            return redirect()->back();
        }
        HelpController::flashSession(true, 'Affiliate updated successfully');
        return redirect()->back();
    }

    public function banAffiliate(Request $request){
        $this->validate($request, [
            'affiliate_id'=>'required|integer'
        ]);
        $aff=Affiliate::find($request->affiliate_id);
        if(!$aff){
            HelpController::flashSession(false, 'Affiliate not found');
            return redirect('/admin/affiliates');
        }
        $msg='';
        if($aff->status == 0){
            $msg='Affiliate banned successfully';
            $aff->status = -1;
        }else{
            $msg='Affiliate activated successfully';
            $aff->status = 0;
        }
        if(!$aff->save()){
            HelpController::flashSession(false, 'Error carrying out this process, pls try again');
            return redirect()->back();
        }
        HelpController::flashSession(true, $msg);
        return redirect()->back();
    }

    public function deleteAffiliate(Request $request){
        $this->validate($request, [
            'affiliate_id'=>'required|integer'
        ]);
        $aff=Affiliate::find($request->affiliate_id);
        if(!$aff){
            HelpController::flashSession(false, 'Affiliate not found');
            return redirect('/admin/affiliates');
        }
        $aff->delete();
        HelpController::flashSession(true, 'Affiliate deleted successfully');
        return redirect('/admin/affiliates');
    }

    public function affiliatePercentage(){
        $bonus = DB::table('trade_config')->where('name', 'Affiliate percentage')->first();
        return view('admin.affiliate.affiliate_percent')->with('bonus',$bonus);
    }

    public function updateAffiliatePercentage(Request $request){
        $this->validate($request, [
            'bonus_id'=>'required|integer',
            'percentage'=>'required|numeric|between:0,99.99'
        ]);
        DB::table('trade_config')->where('id', $request->bonus_id)->update([
            'value'=>$request->percentage
        ]);
        HelpController::flashSession(true, 'Affiliate bonus percentage updated successfully');
        return back();
    }

    public function profitSharing(){
        $pss = DB::table('bonus')->get();
        return view('admin.affiliate.profit_sharing')->with('pss',$pss);
    }

    public function addProfitSharing(Request $request){
        $this->validate($request, [
            'name'=>'required|string',
            'percentage'=>'required|regex:/^\d*(\.\d{2})?$/',
        ]);
        
        DB::table('bonus')->insert([
            'name'=>$request->name,
            'percentage'=>$request->percentage
        ]);
        HelpController::flashSession(true, 'Profit sharing commission');
        return back();
    }
    
    public function updateProfitSharing(Request $request){
        $this->validate($request, [
            'share_id'=>'required|integer',
            'name'=>'required|string',
            'percentage'=>'required|regex:/^\d*(\.\d{2})?$/',
        ]);
        
        DB::table('bonus')->where('id', $request->share_id)->update([
            'name'=>$request->name,
            'percentage'=>$request->percentage
        ]);
        HelpController::flashSession(true, 'Profit sharing commission updated successfully');
        return back();
    }

    public function deleteProfitSharing(Request $request){
        $this->validate($request, [
            'share_id'=>'required|integer'
        ]);
        DB::table('bonus')->where('id', $request->share_id)->delete();
        HelpController::flashSession(true, 'Profit sharing deleted successfully');

        return back();
    }

}
