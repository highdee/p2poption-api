<?php

namespace App\Http\Controllers\AdminController;
use Illuminate\Support\Facades\App;

use App\Affiliate;
use App\assets;
use App\AssetTiming;
use App\AssetType;
use App\Chat;
use App\Http\Controllers\Controller;
use App\Jobs\verify_rejected;
use App\User;
use App\Verification;
use Illuminate\Http\Request;
use Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Session;

class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function createAssets(Request $request,$type){
        $this->validate($request, [
            'asset'=>'string|required',
        ]);

        //UNPACKAGING THE JSON STRING OBJECT FROM BINANCE
        $asset = json_decode($request->asset);
        $baseAsset='';
        $quoteAsset='';

        if(isset($asset->baseAsset)){
            $baseAsset=$asset->baseAsset;
            $quoteAsset=$asset->quoteAsset;
        }else if(isset($asset->currency)){
            $currencies=explode('/',$asset->currency);
            $baseAsset=$currencies[0];
            $quoteAsset=$currencies[1];
        }


        //CHECKING IF ASSET EXIST
        $check_asset = assets::where(['base_asset' => $baseAsset, 'quote_asset' =>  $quoteAsset])->first();
        if($check_asset){
            HelpController::flashSession(false, 'Asset already exist');
            return redirect()->back();
        }

        //SAVING NEW ASSET
        $Asset=new assets();
        $Asset->base_asset=$baseAsset;
        $Asset->quote_asset=$quoteAsset;
        $Asset->type_id=$type;
        $Asset->save();



        //VALIDATING THE PERCENTAGES
        $keys=array_keys($request->except(['_token','asset']));
        $validated=[];
        foreach ($keys as $key){
            if($key != 'base_asset' && $key != 'quote_asset' && $key != '_token'){
                if(!preg_match('/^[0-9]+(\.[0-9]{1,2})?$/', $request->get($key))){
                    HelpController::flashSession(false, 'Invalid percentage value');
                    return redirect()->back();
                }
                if($request->get($key) < 0 || $request->get($key) > 100){
                    HelpController::flashSession(false, 'Percentage value should not be greater than 100 or less than 1');
                    return redirect()->back();
                }
                $validated[$key]=$request->get($key);
            }
        }

        //ATTACHING THE ASSETS AND TIME AND PERCENTAGES
        foreach ($validated as $time_id => $percentage){
            $Asset->timing()->syncWithoutDetaching([
                $time_id=>[ 'percentage'=>$percentage ]
            ]);
        }

        HelpController::flashSession(true, 'Assets created successfully');
        return redirect()->back();
    }

    // public function depositBonus(){
    //     $bonus = DB::table('trade_config')->where('name', 'Deposit bonus')->first();
    //     return view('admin.deposit_bonus')->with('bonus',$bonus);
    // }

    public function updateDepositBonus(Request $request){
        $this->validate($request, [
            'bonus_id'=>'required|integer',
            'percentage'=>'required|numeric|between:0,99.99'
        ]);
        DB::table('trade_config')->where('id', $request->bonus_id)->update([
            'value'=>$request->percentage
        ]);
        HelpController::flashSession(true, 'Deposit bonus percentage updated successfully');
        return back();
    }

    public function language(Request $request){
        $this->validate($request, [
            'lang'=>'string|required'
        ]);
        if (! in_array($request->lang, ['en','kr'])) {
            Session::put('locale','en');
            App::setLocale('en');
        }else{
            Session::put('locale',$request->lang);
            App::setLocale($request->lang);
        }
        return back();
    }

    public function showAsset($id){
        $asset=assets::find($id);
        if(!$asset){
            HelpController::flashSession(false, 'Asset not found');
            return redirect('/admin/assets');
        }
//        dd($asset->timing[0]);
        return view('admin.assets.asset')->with('asset', $asset);
    }

    public function allAssets(){
        $asset=assets::all();
        $types=AssetType::where('id',1)->get();


        return view('admin.assets.index')->with([
            'assets'=> $asset,
            'types' => $types,
        ]);
    }

    public function createAssetPage($id){
        $assetType=AssetType::findOrFail($id);
        $symbols=[];

        if($assetType->title == 'Crypto'){//Fetch symbols from BINANCE
            $response = Http::get('https://api.binance.com/api/v3/exchangeInfo');
            if($response->status() == 200){
                $result=json_decode($response->body());
                $symbols=$result->symbols;
            }
        }elseif ($assetType->title == 'Forex'){//Fetch symbols from LIVE RATE
            $response = Http::get('https://live-rates.com/rates?key='.env('LIVE_RATE_KEY'));
            if($response->status() == 200){
                $result=json_decode($response->body());
                $symbols=$result;
            }
        }

        $times=AssetTiming::where('status', 1)->get();

        return view('admin.assets.create')->with([
            'times' => $times,
            'symbols' => $symbols,
            'type_id'  => $id,
            'type' =>$assetType
        ]);
    }

    public function updateAssets(Request $request){
        $this->validate($request, [
            'asset_id'=>'required|integer',
//            'percentage'=>'required|regex:/^\d*(\.\d{2})?$/'
        ]);
        $asset=assets::find($request->asset_id);
        if(!$asset){
            HelpController::flashSession(false, 'Asset not found');
            return redirect('/admin/assets');
        }
        if($request->status){
            $asset->status = 1;
        }else{
            $asset->status = 0;
        }

        if(!$asset->save()){
            HelpController::flashSession(false, 'Error updating asset, please try again');
            return redirect()->back();
        }

        $keys=array_keys($request->input());
        $hold=[];
        foreach ($keys as $key){
            if($key != 'base_asset' && $key != 'quote_asset' && $key != '_token' && $key != 'asset_id' && $key != 'status'){
                if(!preg_match('/^[0-9]+(\.[0-9]{1,2})?$/', $request->get($key))){
                    HelpController::flashSession(false, 'Invalid percentage value');
                    return redirect()->back();
                }
                if($request->get($key) < 0 || $request->get($key) > 100){
                    HelpController::flashSession(false, 'Percentage value should not be greater than 100 or less than 1');
                    return redirect()->back();
                }
                $hold[$key]=$request->get($key);
            }
        }
        foreach ($hold as $time_id => $percentage){
            $asset->timing()->syncWithoutDetaching([
                $time_id=>[
                    'percentage'=>$percentage
                ]
            ]);
        }

        HelpController::flashSession(true, 'Asset updated successfully');
        return redirect()->back();
    }

    public function toggleAssetStatus(Request $request){
        $this->validate($request, [
            'asset_id'=>'required|integer'
        ]);
        $asset=assets::find($request->asset_id);
        if(!$asset){
            HelpController::flashSession(false, 'Asset not found');
            return redirect('/admin/assets');
        }
        if($request->status){
            $msg=$asset->base_asset.'/'.$asset->quote_asset .' has been activated successfully';
            $asset->status = 1;

        }else{
            $msg=$asset->base_asset.'/'.$asset->quote_asset .' has been deactivated successfully';
            $asset->status = 0;
        }
        if(!$asset->save()){
            HelpController::flashSession(false, 'Error updating asset status, please try again');
            return redirect()->back();
        }
        HelpController::flashSession(true, $msg);
        return redirect()->back();
    }

    public function deleteAsset(Request $request){
        $this->validate($request, [
            'asset_id'=>'required|integer'
        ]);
        $asset=assets::find($request->asset_id);
        $asset->delete();
        HelpController::flashSession(true, 'Asset deleted successfully');
        return redirect('/admin/assets');
    }

    public function createTimingPage(){
        return view('admin.assets.create_timing');
    }

    public function timings(){
        $times=AssetTiming::all();
        return view('admin.assets.timings')->with('times',$times);
    }

    public function createTiming(Request $request){
        $this->validate($request, [
           'name'=>'required|integer|unique:asset_timings'
        ]);
        $time=new AssetTiming();
        $time->name=$request->name;
        if(!$time->save()){
            HelpController::flashSession(false, 'Error saving timing, please try again');
            return redirect()->back();
        }
        HelpController::flashSession(true, 'Timing saved successfully');
        return redirect()->back();
    }

    public function toggleTiming(Request $request){
        $this->validate($request, [
            'time_id'=>'required|integer'
        ]);
        $time=AssetTiming::find($request->time_id);
        if(!$time){
            HelpController::flashSession(false, 'Timing not found');
            return redirect()->back();
        }
        if($time->status == 0){
            $time->status = 1;
            $msg= $time->name .' seconds activated successfully';
        }else{
            $time->status = 0;
            $msg= $time->name .' seconds deactivated successfully';
        }
        if(!$time->save()){
            HelpController::flashSession(false, 'Error updating timing status, please try again');
            return redirect()->back();
        }
        HelpController::flashSession(true, $msg);
        return redirect()->back();
    }

    public function chats(){
        $chats=Chat::orderBy('created_at', 'desc')
                    ->where('sender','!=',0)
                    ->get();
        $unique=[];
        $data=[];
        foreach ($chats as $chat){
            if(!in_array($chat->sender, $unique)){
                array_push($unique, $chat->sender);
                array_push($data, $chat);
            }
        }
        return view('admin.chats.index')->with('chats',$data);
    }

    public function sendMessage(Request $request){
        $this->validate($request, [
            'receiver'=>'integer|required',
            'message'=>'string|required',
        ]);
        $chat=new Chat();
        $chat->sender=0;
        $chat->receiver=$request->receiver;
        $chat->message=$request->message;
        $chat->save();
        HelpController::flashSession('true', 'Message sent successfully');
        return redirect()->back();
    }

    public function chat($id){
        $user=User::find($id);
        $chats=Chat::where('sender',$id)
                    ->orWhere('receiver',$id)
                    ->get();
        if(!$user){
            HelpController::flashSession(false, 'User not found or user has been deleted');
            return redirect()->back();
        }
        return view('admin.chats.chat')->with(['user'=>$user,'chats'=>$chats]);
    }

    public function index(){
        $user=User::all()->count();
        $aff=Affiliate::all()->count();
        return view('admin.dashboard')->with(['user'=>$user,'aff'=>$aff]);
    }

    public function users(){
        $users=User::all()->sortByDesc('id');
        return view('admin.users')->with('users',$users);
    }

    public function verificationRequest(){
        $list=Verification::where('identity_status',0)
                            ->orWhere('address_status',0)
                            ->get();
        return view('admin.verification')->with('lists',$list);
    }

    public function verifyIdentity(Request $request){
        $this->validate($request, [
            'user_id'=>'required|integer',
        ]);
        $user=Verification::where('user_id',$request->user_id)->first();
        $user->identity_verified_at=date('Y-m-d');
        $user->identity_status=1;
        if(!$user->save()){
            HelpController::flashSession(false, 'Error verifying identity, pls try again');
            return redirect()->back();
        }
        HelpController::flashSession(true, 'Identity verified successfully');
        return redirect()->back();
    }

    public function verifyAddress(Request $request){
        $this->validate($request, [
            'user_id'=>'required|integer',
        ]);
        $user=Verification::where('user_id',$request->user_id)->first();
        $user->address_verified_at=date('Y-m-d');
        $user->address_status=1;
        if(!$user->save()){
            HelpController::flashSession(false, 'Error verifying address, pls try again');
            return redirect()->back();
        }
        HelpController::flashSession(true, 'Address verified successfully');
        return redirect()->back();
    }
    public function rejectAddress(Request $request){
        $this->validate($request, [
            'user_id'=>'required|integer',
        ]);
        $user=Verification::where('user_id',$request->user_id)->first();
        $user->address_status = -1;
        $user->address_verification = null;
        if(!$user->save()){
            HelpController::flashSession(false, 'Error rejecting address, pls try again');
            return redirect()->back();
        }
        $data=[
            'type'=>'Address',
            'email'=>$user->user->email
        ];
        verify_rejected::dispatch($data);
        HelpController::flashSession(true, 'Address rejected successfully');
        return redirect()->back();
    }

    public function rejectIdentity(Request $request){
        $this->validate($request, [
            'user_id'=>'required|integer',
        ]);
        $user=Verification::where('user_id',$request->user_id)->first();
        $user->identity_status = -1;
        $user->identity_verification=null;
        if(!$user->save()){
            HelpController::flashSession(false, 'Error rejecting identity, pls try again');
            return redirect()->back();
        }
        $data=[
            'type'=>'Identity',
            'email'=>$user->user->email
        ];
        verify_rejected::dispatch($data);
        HelpController::flashSession(true, 'Identity rejected successfully');
        return redirect()->back();
    }

    public function user($id){
        $user=User::find($id);
        if(!$user){
            HelpController::flashSession(false, 'User not found');
            return redirect('admin/users');
        }
        return view('admin.user')->with('user',$user);
    }

    public function updateUser(Request $request){
        $this->validate($request, [
            'user_id'=>'required|integer',
            'email'=>'required|email'
        ]);
        $user=User::find($request->user_id);
        if(isset($request->firstname)){
            $user->firstname=$request->firstname;
        }
        if(isset($request->lastname)){
            $user->lastname=$request->lastname;
        }
        if(isset($request->firstname)){
            $user->firstname=$request->firstname;
        }
        if(isset($request->phone)){
            $user->phone=$request->phone;
        }
        if(isset($request->address)){
            $user->address=$request->address;
        }
        if(isset($request->address_two)){
            $user->address_two=$request->address_two;
        }

        if(!$user->save()){
            HelpController::flashSession(false, 'Error updating user');
            return redirect()->back();
        }
        HelpController::flashSession(true, 'User updated successfully');
        return redirect()->back();
    }
    public function banUser(Request $request){
        $this->validate($request, [
            'user_id'=>'required|integer'
        ]);
        $user=User::find($request->user_id);
        if(!$user){
            HelpController::flashSession(false, 'User not found');
            return redirect('/admin/users');
        }
        $msg='';
        if($user->status == 0){
            $msg='User banned successfully';
            $user->status = -1;
        }else{
            $msg='User activated successfully';
            $user->status = 0;
        }
        if(!$user->save()){
            HelpController::flashSession(false, 'Error carrying out this process, pls try again');
            return redirect()->back();
        }
        HelpController::flashSession(true, $msg);
        return redirect()->back();
    }
    public function deleteUser(Request $request){
        $this->validate($request, [
            'user_id'=>'required|integer'
        ]);
        $user=User::find($request->user_id);
        if(!$user){
            HelpController::flashSession(false, 'User not found');
            return redirect('/admin/affiliates');
        }
        $user->delete();
        HelpController::flashSession(true, 'User deleted successfully');
        return redirect('/admin/user');
    }

    public function logout(){
        session()->flush();
        Auth::logout();
        return redirect()->route('login.admin');
    }
}
