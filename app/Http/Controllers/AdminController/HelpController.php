<?php

namespace App\Http\Controllers\AdminController;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class HelpController extends Controller
{
    public static function flashSession($status, $msg){
        session()->flash('status', $status);
        session()->flash('msg', $msg);
    }
}
