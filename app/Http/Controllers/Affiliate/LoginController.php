<?php

namespace App\Http\Controllers\Affiliate;

use App\Affiliate;
use App\AffiliateWallet;
use App\Http\Controllers\Controller;
use App\Jobs\verificationMailJob;
use App\Mail\resetMail;
use App\Referral;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;
use Auth;

class LoginController extends Controller
{
    public function IncreaseVisitor(Request $request){
        $token=$request->query('referral');
        $aff=Referral::where('token', $token)->first();
        if($aff){
            DB::table('affiliate_visitors')
                ->where('affiliate_id', $aff->affiliate_id)
                ->increment('number',1);
        }
    }

    public function login(Request $request){
        $this->validate($request,[
            'email'=>'required|email',
            'password'=>'required'
        ]);

        $credentials = $request->only('email', 'password');
        if($token=Auth('affiliate')->attempt($credentials)){
            $user=Auth('affiliate')->user($token);
            return response()->json([
                'status'=>true,
                'msg'=>"Login Successful",
                'token'=>$token,
                "user"=>$user
            ]);
        }else{
            return response()->json([
                'status'=>false,
                'msg'=>'Invalid login credentials'
            ]);
        }
    }

    public function createAccount(Request $request){
        $this->validate($request,[
            'email'=>'required|email|unique:affiliates',
            'password'=>'required|confirmed|min:6',
        ]);

        $code=substr(sha1(time()), 0, 14);

        $aff = new Affiliate();
        $aff->email = $request['email'];
        $aff->password = bcrypt($request['password']);
        $aff->referral_token=$code;
        $aff->referral_link=env('APP_FRONTEND').'/referral/'. $code;
        $aff->remember_token=mt_rand(99999,99999999).Str::random(12).mt_rand(99999,99999999).Str::random(12);
        $aff->save();

        $referral= new Referral();

        $referral->token=$code;
        $referral->link=env('APP_FRONTEND').'/referral/'. $code;
        $referral->promocode=1;
        $referral->offer_type=3;
        $referral->cashback=$referral->DepositPercentage;
        $referral->affiliate_id=$aff->id;
        $referral->save();

        $wallet=new AffiliateWallet();
        $wallet->affiliate_id = $aff->id;
        $wallet->amount=0;
        $wallet->status=0;
        $wallet->save();

        DB::table('affiliate_visitors')->insert([
            'affiliate_id'=>$aff->id,
            'number'=>0
        ]);

        $token=Auth('affiliate')->login($aff);
        return response()->json([
            'status'=>true,
            'message'=>'Registration Successful',
            'token'=>$token,
            'user'=>$aff
        ]);
    }

    public function forget_password(Request $request){
        $this->validate($request,[
            'email'=>'required',
        ]);

        $aff=Affiliate::where('email',$request->input('email'))->first();

        if(!$aff){
            return response()->json([
                'status'=>false,
                'msg'=>"Account not found"
            ]);
        }

        $aff->remember_token=mt_rand(99999,99999999).Str::random(12).mt_rand(99999,99999999).Str::random(12);
        $aff->save();

        $link=env('APP_FRONTEND')."/affiliate/recover-password/".$aff->remember_token;
        Mail::to($aff->email)->send(new resetMail([
            'link'=> $link
        ]));

        return response()->json([
            'status'=>true,
            'msg'=>"Reset mail has been sent"
        ]);
    }

    public function verify_password(Request $request){
        $token=$request->query('code');

        if(!isset($token)){
            return response()->json(['status'=>false,'msg'=>"Token not found" ]);
        }

        $aff=Affiliate::where('remember_token',$token)->first();

        if(!$aff){
            return response()->json([
                'status'=>false,
                'msg'=>"Expired link"
            ]);
        }

        return response()->json(['status'=>true,'msg'=>"correct token" ]);
    }

    public function reset_password(Request $request){
        $this->validate($request,[
            'password'=>'required|confirmed',
            'token'=>'required'
        ]);

        $aff=Affiliate::where('remember_token',$request->input('token'))->first();

        if(!$aff){
            return response()->json([
                'status'=>false,
                'msg'=>"Account not found"
            ]);
        }

        $aff->password=bcrypt($request->input('password'));
        $aff->remember_token=mt_rand(99999,99999999).Str::random(12).mt_rand(99999,99999999).Str::random(12);
        $aff->save();

        return response()->json(['status'=>true,'msg'=>"Password reset was successful" ]);
    }
}
