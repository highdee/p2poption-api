<?php

namespace App\Http\Controllers\Affiliate;

use App\Http\Controllers\Controller;
use App\Referral;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use App\Affiliate;
use Illuminate\Support\Facades\DB;

class AffiliateController extends Controller
{
    public function __construct()
    {
        $this->middleware('jwt.auth');
        Config::set('auth.providers.users.model', \App\Affiliate::class);
    }

    public function updateAffiliate(Request $request){
        $user=Auth('affiliate')->user($request->token);
        if(!$user){
            return response([
                'status'=>false,
                'message'=>'Affiliate not found'
            ]);
        }
        if(isset($request->firstname)){
            $user->firstname=$request->firstname;
        }
        if(isset($request->lastname)){
            $user->lastname=$request->lastname;
        }
        if(isset($request->username)){
            $user->username=$request->username;
        }
        if(isset($request->country)){
            $user->country=$request->country;
        }
        if(isset($request->phone)){
            $user->phone=$request->phone;
        }
        if(isset($request->traffic_source)){
            $user->traffic_source=$request->traffic_source;
        }
        if(!$user->save()){
            return response([
                'status'=>false,
                'message'=>'Error updating affiliate'
            ]);
        }
        return response([
            'status'=>true,
            'message'=>'Affiliate updated successfully',
            'user'=>$user,
        ]);
    }

    public function getAffiliateDashboard(Request $request){
        $aff=Affiliate::where('id',Auth('affiliate')->user($request->query('token'))->id)->first();
        if(!$aff){
            return response()->json([
                'status'=>false,
                'msg'=>'Token expired, pls login again'
            ]);
        }
        $referral=Referral::where('affiliate_id', $aff->id)->get();
        return response([
            'status'=>true,
            'data'=>$aff,
            'referral'=>$referral
        ],200);
    }

    public function getBonus(){
        $bonus=DB::table('bonus')->get();
        return response([
            'status'=>true,
            'data'=>$bonus
        ]);
    }


}
