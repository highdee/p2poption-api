<?php

namespace App\Http\Controllers\Affiliate;

use App\Affiliate;
use App\Http\Controllers\Controller;
use App\Referral;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;

class ReferralController extends Controller
{
    public function __construct()
    {
        $this->middleware('jwt.auth');
        Config::set('auth.providers.users.model', \App\Affiliate::class);
    }

    public function createAffiliateLink(Request $request){
        $this->validate($request, [
            'token'=>'required|string|unique:referrals|min:5',
            'promo_code'=>'required|numeric|between:0,99.99'
            ],
            ['token.unique'=>'Campaign name is already taken']
        );
        $aff=Affiliate::where('id',Auth('affiliate')->user($request->query('token'))->id)->first();
        if(!$aff){
            return response()->json([
                'status'=>false,
                'msg'=>'Token expired, pls login again'
            ]);
        }
        $referral=new Referral();
        $referral->affiliate_id=$aff->id;
        $referral->token=$request->token;
        $referral->link=env('APP_FRONTEND').'/referral/'.$request->token;
        $referral->promocode=$request->promo_code;
//        $referral->offer_type=$request->offer_type;
        if($referral->save()){
            return response([
                'status'=>true,
                'msg'=>'Affiliate link created successfully'
            ], 200);
        }else{
            return response([
                'status'=>false,
                'msg'=>'Error creating affiliate link'
            ]);
        }
    }

    public function getAffiliateBonus(){
        $bonus = DB::table('trade_config')->where('name','Affiliate percentage')->first();
        return response([
            'status'=>true,
            'data'=>$bonus
        ]);
    }

    public function deleteAffiliateLink(Request $request){
        $this->validate($request, [
            'referral_token'=>'required|string'
        ]);
        $ref=Referral::where('token',$request->referral_token)->first();
        if(!$ref){
            return response([
                'status'=>false,
                'msg'=>'Affiliate token not found, pls try again'
            ]);
        }
        $aff=Affiliate::where('referral_token', $request->referral_token)->first();
        if($aff){
            return response([
                'status'=>false,
                'msg'=>'You can\'t delete your default affiliate link'
            ]);
        }
        if(!$ref->delete()){
            return response([
                'status'=>false,
                'msg'=>'Error deleting affiliate link, pls try again'
            ]);
        }
        return response([
            'status'=>true,
            'msg'=>'Affiliate link deleted successfully'
        ]);
    }

    public function calculateAffiliateProfit($user_id, $amount, $action){
        $user=User::find($user_id);

    }
}
