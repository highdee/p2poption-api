<?php

namespace App\Http\Controllers;

use App\AssetTiming;
use App\AssetType;
use App\DemoWallet;
use App\TradingHistory;
use App\User;
use App\Wallet;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class TradingController extends Controller
{
    public function getSymbols(){
        $assetTypes = AssetType::all();
        $timing = AssetTiming::where('status',1)->get();

        return response(['types'=>$assetTypes,'timing'=>$timing]                                   ,200);
    }

    public function updateTradingInfo(Request $request){
        $details=$request->input();
        $user=Auth::user($request->query('token'));

        $history=new TradingHistory();
        $history->user_id=$user->id;
        $history->side=$details['side'];
        $history->account_type=$details['account_type'];
        $history->order_id=$this->getOrderID() .'-'.$this->getOrderID().'-'.$this->getOrderID();
        $history->asset=$details['asset'];
        $history->open_time=$details['open_time'];
        $history->close_time=$details['close_time'];
        $history->open_price=$details['open_price'];
        $history->close_price=$details['close_price'];
        $history->amount=$details['amount'];
        $history->profit=$details['profit'];
        $history->percentage=$details['percentage'];
        $history->seconds=$details['seconds'];
        $history->save();



        if($history->account_type != 'wallet'){
            $demo=DemoWallet::where('user_id',$user->id)->first();

            if($demo){
                if($demo->amount >= $details['amount']){
                    $demo->amount += $details['profit'] < 0 ? 0 : ($details['profit']+$details['amount']);
                    $demo->save();
                }
            }
        }else {
            $wallet=Wallet::where('user_id',$user->id)->first();
            if($wallet){
                if($wallet->amount >= $details['amount']){
                    $wallet->amount += $details['profit'] < 0 ? 0 : ($details['profit']+$details['amount']);
                    $wallet->save();
                }

            }
        }

        $user=User::find($user->id);

        return response([
            'status'    => true,
            'data'  => [
                'history' => $history,
                'user'  => $user
            ]
        ],200);

    }

    public function getOrderID(){
        $random="0A1B2C3D4E5F6G7I7J8K9L1M2N3O4P5Q6R7S8T9U0VW6X7Y3Z9";
        return $random[mt_rand(0,strlen($random)-1)].$random[mt_rand(0,strlen($random)-1)].$random[mt_rand(0,strlen($random)-1)].$random[mt_rand(0,strlen($random)-1)].$random[mt_rand(0,strlen($random)-1)];
    }

    public function creditDemoWallet(Request $request){
        $user=Auth::user();

        $details=$request->input();

//        IF VARIABLE IS NOT SET
        if(!isset($details['amount'])){
            return response([
                'status'=>false,
                'message'=>"Amount field is required"
            ],200);
        }

//      VALIDATION FOR AMOUNT
        if($details['amount'] == 0){
            return response([
                'status'=>false,
                'message'=>"Amount is too small"
            ],200);
        }

        $DemoWallet=DemoWallet::where('user_id',$user->id)->first();

        if($DemoWallet){
            $DemoWallet->amount += $details['amount'];
            $DemoWallet->save();

            $user=User::find($user->id);
            return response([
                'status'=>true,
                'message'=>"Demo account was credited successfully",
                'data'=>$user
            ],200);
        }

        return response([
            'status'=>false,
            'message'=>""
        ],404);
    }

//    FETCH DEMO ACCOUNT HISTORY
    public function walletHistory(){
        $user = Auth::user();
        $account_type = 'wallet';
        $histories = TradingHistory::where(['user_id'=> $user->id])->orderBy('id','desc')->where('account_type' , $account_type)->paginate(50);

        return $histories;
    }

    //    FETCH DEMO WALLET ACCOUNT HISTORY
    public function demoWalletHistory(){
        $user = Auth::user();
        $account_type = 'demo_wallet';
        $demoWallet_histories = TradingHistory::where(['user_id'=> $user->id])->orderBy('id','desc')->where('account_type' , $account_type)->paginate(50);

        return $demoWallet_histories;
    }

    //    FETCH ALL WALLET ACCOUNT HISTORY
    public function allTradingHistory(){
        $user = Auth::user();
        $histories = TradingHistory::where(['user_id'=> $user->id])->orderBy('id','desc')->paginate(50);

        return $histories;
    }







//    QUICK DEMO TRADING
    public function quickUpdateTradingInfo(Request $request){
        $details=$request->input();
        $ip = $request->getClientIp();

        $history=DB::table('quick_trading_histories')->insert([
            'ip_address'=>$ip,
            'side'=>$details['side'],
            'account_type'=>$details['account_type'],
            'order_id'=>$this->getOrderID() .'-'.$this->getOrderID().'-'.$this->getOrderID(),
            'asset'=>$details['asset'],
            'open_time'=>$details['open_time'],
            'close_time'=>$details['close_time'],
            'open_price'=>$details['open_price'],
            'close_price'=>$details['close_price'],
            'amount'=>$details['amount'],
            'profit'=>$details['profit'],
            'percentage'=>$details['percentage'],
            'seconds'=>$details['seconds'],
            'created_at'=>date('y-m-d H:i:s')
        ]);

        return response([
            'status'    => true,
            'data'  => [
                'history' => DB::table('quick_trading_histories')->where('ip_address',$ip)->orderBy('id','desc')->first()
            ]
        ],200);
    }
    public function quickTradingHistory(Request $request){
        $ip =  $request->getClientIp();
        $histories = DB::table('quick_trading_histories')->where('ip_address',$ip)->orderBy('id','desc')->paginate(50);

        return $histories;
    }
}
