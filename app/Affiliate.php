<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\DB;
use Tymon\JWTAuth\Contracts\JWTSubject;
use \Illuminate\Database\Eloquent\SoftDeletes;

class Affiliate extends Authenticatable implements JWTSubject
{
    use SoftDeletes;

//    1-Volume sharing
//    2-Turnover sharing
//    3-Deposit sharing
//    4-Revenue sharing
//    5-CPA

//          Cashback
//  1=0%
//  2=10%
//  3=20%
//  4=30%
//  5=40%

    protected $guard='affiliate';
    protected $appends=['count_referral','visitors'];


    public function getCountReferralAttribute(){
        $count=0;
        $getRef=$this->referrals()->get();
        foreach ($getRef as $get){
            $ref_user=DB::table('affiliate_referral_user')->where('referral_id', $get->id)->count();
            $count = $count + $ref_user;
        }
        return $count;
    }
    public function getVisitorsAttribute(){
        $visitor=DB::table('affiliate_visitors')->where('affiliate_id',$this->id)->first();
        return $visitor->number;
    }

    public function referrals(){
        return $this->hasMany('App\Referral');
    }

    public function wallet(){
        return $this->hasOne('App\AffiliateWallet');
    }

    public function users(){
        return $this->belongsToMany('App\User','affiliate_referral_user','affiliate_id','user_id');
    }

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    public function getJWTCustomClaims()
    {
        return [];
    }
}
