<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Wallet extends Model
{
    protected $hidden=['created_at','updated_at','id'];

    protected $appends = ['amount_usd'];

    public function getamountUsdAttribute(){
        return $this->amount * 100;
    }
}
