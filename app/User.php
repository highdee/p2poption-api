<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable;
    use SoftDeletes;

    protected $appends=['hasActiveWithdrawal','verification','wallet','demo_wallet','login_sessions'];


    protected $fillable = [
        'email', 'password',
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function getHasActiveWithdrawalAttribute(){
        $w_reqs = WithdrawalRequest::where([
            'user_id' => $this->id,
            'accepted_at' => null,
            'rejected_at' => null,
        ])->first();

        return $w_reqs;
    }

    public function getLoginSessionsAttribute(){
        return $this->loginHistories()->where('expired_at',null)->get();
    }

    public function getWalletAttribute(){
        $wallet=$this->hasOne('App\Wallet')->first();
        if(!$wallet){
            $wallet=new Wallet();
            $wallet->user_id=$this->id;
            $wallet->amount=0;
            $wallet->save();
        }

        return $wallet;
    }

    public function getDemoWalletAttribute(){
        $wallet=$this->hasOne('App\DemoWallet')->first();

        if(!$wallet){
            $wallet=new DemoWallet();
            $wallet->user_id=$this->id;
            $wallet->amount=0;
            $wallet->save();
        }

        return $wallet;
    }

    public function getVerificationAttribute(){
        return $this->verify()->first();
    }

    public function loginHistories(){
        return $this->hasMany('App\LoginHistory');
    }

    public function chats(){
        return $this->hasMany('App\Chat','sender','id');
    }

    public function wallet(){
        $this->hasOne('App\Wallet');
    }
    public function verify(){
        return $this->hasOne('App\Verification');
    }

    public function referral(){
        return $this->belongsToMany('App\Referral','affiliate_referral_user','user_id','referral_id');
    }
    public function demo_wallet(){
        return $this->hasOne('App\DemoWallet');
    }

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    public function getJWTCustomClaims()
    {
        return [];
    }
}
