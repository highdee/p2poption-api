<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TradingHistory extends Model
{
    //

    protected $appends=['time','date','purchase_time','expire_time'];


    protected function getPurchaseTimeAttribute(){
        return date('d-M - H:i:s',strtotime($this->open_time));
    }
    protected function getExpireTimeAttribute(){
        return date('d-M - H:i:s',strtotime($this->close_time));
    }

    protected function getTimeAttribute(){
        return date('H:i',strtotime($this->created_at));
    }
    protected function getDateAttribute(){
        return date('d M',strtotime($this->created_at));
    }
}
