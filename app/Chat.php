<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Chat extends Model
{
    protected $casts = [
        'created_at' => 'datetime:D, m-Y',
    ];
    public function user(){
        return $this->belongsTo('App\User','sender','id');
    }
}
