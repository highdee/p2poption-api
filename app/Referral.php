<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;

class Referral extends Model
{
    use softDeletes;

//    1-Volume sharing
//    2-Turnover sharing
//    3-Deposit sharing
//    4-Revenue sharing
//    5-CPA

//          Cashback
//  1=0%
//  2=10%
//  3=20%
//  4=30%
//  5=40%

    protected $appends=['referral_users'];
    public $RevenueSharingPercentage=25;
    public $CpaPercentage=15;
    public $DepositPercentage=35;

    public function getReferralUsersAttribute(){
        return $this->users();
    }
    public function users(){
        return DB::table('affiliate_referral_user')->where('referral_id', $this-> id)->get();
    }
    public function affiliate(){
        return $this->belongsTo('App\Affiliate');
    }
//    public function getPromocodeAttribute($value){
//        $name='';
//        if($value == 1){
//            $name='50START (bonus 50%; min deposit $50)';
//        }elseif ($value==2){
//            $name='1 MORE CHANCE (bonus 50%; min deposit $100)';
//        }else{
//            $name='';
//        }
//        return $name;
//    }
    public function getOfferTypeAttribute($value){
        $name='';
        if($value == 1){
            $name='Volume sharing';
        }elseif ($value==2){
            $name='Turnover sharing';
        }elseif ($value==3){
            $name='Deposit sharing';
        }elseif ($value==4){
            $name='Revenue sharing';
        }else{
            $name='CPA';
        }
        return $name;
    }

}
