@extends('inc.app')
@section('exchange-rates')
    active
@endsection
@section('content')
    <div id="content-page" class="content-page">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12 px-0">
                    <div class="iq-card">
                        <div class="iq-card-header d-flex justify-content-between align-items-center">
                            <div class="iq-header-title">
                                <h4 class="card-title">{{__('admin.extra_ex')}}</h4>
                            </div>
                        </div>
                        <div class="iq-card-body pt-0">
                            <div class="table-responsive">
                                <table id="datatable" class="table table-striped table-hover" >
                                    <thead>
                                    <tr>
                                        <th>{{__('admin.Currency')}}</th>
                                        <th>{{__('admin.percentage')}} %</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach ($rates as $rate)
                                        <tr>
                                            {{-- onclick="window.location.href='/admin/exchange-rate/{{$rate->id}}'" --}}
                                            <td>{{$rate->currency}}</td>
                                            <td>{{$rate->percentage}}</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <th>{{__('admin.Currency')}}</th>
                                        <th>{{__('admin.percentage')}} %</th>
                                    </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection