@extends('inc.app')
@section('bonus_percent') active @endsection
@section('content')
    <div id="content-page" class="content-page">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12 px-0">
                    @include('inc.notification')
                    <div class="iq-card">
                        <div class="iq-card-header">
                            <div class="iq-header-title">
                                <h4 class="card-title mt-3">{{__('admin.depo_bonus')}}</h4>
                            </div>
                        </div>
                        <div class="iq-card-body pt-0">
                            <div class="table-responsive">
                                <table id="datatable" class="table table-striped table-hover" >
                                    <thead>
                                        <tr>
                                            <th>{{__('admin.name')}}</th>
                                            <th>{{__('admin.percentage')}}</th>
                                            <th>{{__('admin.edit')}}</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>{{$bonus->name}}</td>
                                            <td>{{$bonus->value}}%</td>
                                            <td>
                                                <i class="fa fa-edit font-size-5" data-toggle="modal" data-target="#edit{{$bonus->id}}"></i>
                                            </td>
                                        </tr>
                                        <div class="modal fade" id="edit{{$bonus->id}}" tabindex="-1" role="dialog" aria-labelledby="edit{{$bonus->id}}" aria-hidden="true">
                                            <div class="modal-dialog modal-dialog-centered" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="edit{{$bonus->id}}">{{__('admin.edit')}} {{__('admin.c_ps')}}</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">×</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <form method="POST" action="/admin/update-deposit-bonus">
                                                            @csrf
                                                            <input type="hidden" name="bonus_id" value="{{$bonus->id}}">
                                                            <div class="form-group">
                                                                <label for="name">{{__('admin.name')}}:</label>
                                                                <input type="text" class="form-control" id="name" disabled value="{{$bonus->name}}">
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="percent">{{__('admin.percentage')}} (%):</label>
                                                                <input type="text" class="form-control" id="percent" name="percentage" value="{{$bonus->value}}">
                                                            </div>
                                                            <div class="form-group">
                                                                <button type="submit" class="btn btn-primary px-5 py-2 clickable">{{__('admin.Update')}}</button>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th>{{__('admin.name')}}</th>
                                            <th>{{__('admin.percentage')}}</th>
                                            <th>{{__('admin.edit')}}</th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection