@extends('inc.app')
@section('deposits') active @endsection
@section('dep') active @endsection
@section('content')
    <div id="content-page" class="content-page">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12 px-0">
                    <div class="iq-card">
                        <div class="iq-card-header d-flex justify-content-between align-items-center">
                            <div class="iq-header-title">
                                <h4 class="card-title">{{__('admin.deposits')}}</h4>
                            </div>
                        </div>
                        <div class="iq-card-body pt-0">
                            <div class="table-responsive">
                                <table id="datatable" class="table table-striped table-hover" >
                                    <thead>
                                        <tr>
                                            <th>{{__('admin.Users')}}</th>
                                            <th>{{__('admin.Currency')}}</th>
                                            <th>{{__('admin.Amount')}}</th>
                                            <th>{{__('admin.Merchant')}}</th>
                                            <th>{{__('admin.Address')}}</th>
                                            <th>{{__('admin.Confirm')}}</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($deposits as $depo)
                                            <tr onclick="">
                                                <td>
                                                    @if($depo->user)
                                                        <span>{{$depo->user->firstname}} {{$depo->user->lastname}}</span>
                                                    @else
                                                        <span>N?A</span>
                                                    @endif
                                                </td>
                                                <td>{{$depo->currency}}</td>
                                                <td>{{$depo->amount}}</td>
                                                <td>{{$depo->merchant_id}}</td>
                                                <td>{{$depo->address}}</td>
                                                <td>{{$depo->confirm}}</td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th>{{__('admin.Users')}}</th>
                                            <th>{{__('admin.Currency')}}</th>
                                            <th>{{__('admin.Amount')}}</th>
                                            <th>{{__('admin.Merchant')}}</th>
                                            <th>{{__('admin.Address')}}</th>
                                            <th>{{__('admin.Confirm')}}</th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection