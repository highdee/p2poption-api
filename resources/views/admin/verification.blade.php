@extends('inc.app')
@section('verify-request')
    active
@endsection
@section('content')
    <div id="content-page" class="content-page">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12 px-0">
                    <div class="iq-card">
                        <div class="iq-card-header d-flex justify-content-between align-items-center">
                            <div class="iq-header-title">
                                <h4 class="card-title">{{__('admin.Users')}}</h4>
                            </div>
                        </div>
                        <div class="iq-card-body pt-0">
                            <div class="table-responsive">
                                <table id="datatable" class="table table-striped table-hover" >
                                    <thead>
                                    <tr>
                                        <th>{{__('admin.First_name')}}</th>
                                        <th>{{__('admin.Last_name')}}</th>
                                        <th>{{__('admin.Email')}}</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach ($lists as $list)
                                        <tr onclick="window.location.href='/admin/user/{{$list->user_id}}'">
                                            <td>{{$list->user->firstname}}</td>
                                            <td>{{$list->user->lastname}}</td>
                                            <td>{{$list->user->email}}</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th>{{__('admin.First_name')}}</th>
                                            <th>{{__('admin.Last_name')}}</th>
                                            <th>{{__('admin.Email')}}</th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection