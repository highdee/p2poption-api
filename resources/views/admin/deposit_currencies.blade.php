@extends('inc.app')
@section('deposits') active @endsection
@section('currencies') active @endsection
@section('content')
    <div id="content-page" class="content-page">
        <div class="container-fluid">
            @include('inc.notification')
            <div class="row">
                <div class="col-sm-12 px-0">
                    <div class="iq-card">
                        <div class="iq-card-header d-flex justify-content-between align-items-center">
                            <div class="iq-header-title">
                                <h4 class="card-title">{{__('admin.deposits')}} {{__('admin.Currency')}}</h4>
                            </div>
                        </div>
                        <div class="iq-card-body pt-0">
                            <div class="table-responsive">
                                <table id="datatable" class="table table-striped table-hover" >
                                    <thead>
                                        <tr>
                                            <th>{{__('admin.Currency')}}</th>
                                            <th>{{__('admin.code')}}</th>
                                            <th>{{__('admin.bonus_per')}}</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @foreach ($currency as $curr)
                                        <tr onclick="window.location.href='/admin/deposit-currency/{{$curr->id}}'">
                                            <td>{{$curr->title}}</td>
                                            <td>{{$curr->code}}</td>
                                            <td>{{$curr->percentage_bonus}}%</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th>{{__('admin.Currency')}}</th>
                                            <th>{{__('admin.code')}}</th>
                                            <th>{{__('admin.bonus_per')}}</th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection