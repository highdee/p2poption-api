@extends('inc.app')
@section('content')
    <div id="content-page" class="content-page">
        <div class="container-fluid">
            <div class="row justify-content-center">
                <div class="col-7">
                    <div class="iq-card">
                        <div class="iq-card-header d-flex justify-content-between">
                            <div class="iq-header-title">
                                <h4 class="card-title font-weight-bold">{{__('admin.Update')}} {{__('admin.deposits')}} {{__('admin.Currency')}}</h4>
                            </div>
                        </div>
                        <div class="iq-card-body">
                            <form method="POST" action="/admin/update-deposit-currency">
                                @include('inc.notification')
                                @csrf
                                <input type="hidden" name="currency_id" value="{{$currency->id}}">
                                <div class="form-group asset">
                                    <label for="ba">{{__('admin.name')}}:</label>
                                    <input type="text" class="form-control" name="base_asset" value="{{$currency->title}}" id="ba" disabled>
                                </div>
                                <div class="form-group asset">
                                    <label for="qa">{{__('admin.code')}}:</label>
                                    <input id="qa" type="text" class="form-control" name="quote_asset" value="{{$currency->code}}" disabled>
                                </div>
                                <div class="form-group asset">
                                    <label for="qa">{{__('admin.bonus_per')}}:</label>
                                    <input id="qa" type="text" class="form-control" name="bonus_percentage" value="{{$currency->percentage_bonus}}">
                                </div>
                                <div class="toggle-btn input-group mb-3">
                                    <label for="status" class="col-12 px-0">{{__('admin.Status')}}:</label>
                                    <label class="switch">
                                        <input type="checkbox" {{$currency->status == 0 ? 'checked' : ''}} name="status">
                                        <span class="slider round"></span>
                                    </label>
                                </div>
                                <button type="submit" class="btn btn-primary px-5 py-2">{{__('admin.Update')}}</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection