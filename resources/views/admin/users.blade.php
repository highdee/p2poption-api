@extends('inc.app')
@section('users')
    active
@endsection
@section('content')
    <div id="content-page" class="content-page">
        <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12 px-0">
                <div class="iq-card">
                    <div class="iq-card-header d-flex justify-content-between align-items-center">
                        <div class="iq-header-title">
                            <h4 class="card-title">{{__('admin.Users')}}</h4>
                        </div>
                    </div>
                    <div class="iq-card-body pt-0">
                        <div class="table-responsive">
                            <table id="arrange-table" class="table table-striped table-hover" >
                                <thead>
                                    <tr>
                                        <th>{{__('admin.First_name')}}</th>
                                        <th>{{__('admin.Last_name')}}</th>
                                        <th>{{__('admin.Email')}}</th>
                                        <th>{{__('admin.Unique_ID')}}</th>
                                        <th>{{__('admin.Phone')}}</th>
                                        <th>{{__('admin.Zip_Code')}}</th>
                                        <th>{{__('admin.Country')}}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($users as $user)
                                        <tr onclick="window.location.href='/admin/user/{{$user->id}}'">
                                            <td>{{$user->firstname}}</td>
                                            <td>{{$user->lastname}}</td>
                                            <td>{{$user->email}}</td>
                                            <td>{{$user->unique_id}}</td>
                                            <td>{{$user->phone}}</td>
                                            <td>{{$user->zip_code}}</td>
                                            <td>{{$user->country}}</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>{{__('admin.First_name')}}</th>
                                        <th>{{__('admin.Last_name')}}</th>
                                        <th>{{__('admin.Email')}}</th>
                                        <th>{{__('admin.Unique_ID')}}</th>
                                        <th>{{__('admin.Phone')}}</th>
                                        <th>{{__('admin.Zip_Code')}}</th>
                                        <th>{{__('admin.Country')}}</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        </div>
    </div>
@endsection