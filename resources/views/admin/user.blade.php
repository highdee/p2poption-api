@extends('inc.app')
@section('content')
    <div id="content-page" class="content-page">
        <div class="container-fluid px-3">
            <div class="student-tabs pt-3 pb-4 d-flex justify-content-between">
                <ul class="d-flex nav nav-pills">
                    <li>
                        <a class="nav-link active" data-toggle="pill" href="#account">
                            {{__('admin.account')}}
                        </a>
                    </li>
                    <li>
                        <a class="nav-link" data-toggle="pill" href="#personal-information">
                            {{__('admin.User_Information')}}
                        </a>
                    </li>
                    <li>
                        <a class="nav-link" data-toggle="pill" href="#verify">
                            {{__('admin.Verify')}}
                        </a>
                    </li>
                </ul>
                <ul>
                    <li>
                        <button type="button" class="credit clickable" data-toggle="modal" data-target="#credit-acc">{{__('admin.Credit_account')}}</button>
                    </li>
                </ul>
            </div>
            <div class="row">
                <div class="col-lg-12 px-0">
                    <div class="iq-edit-list-data">
                        <div class="tab-content">
                            @include('inc.notification')
                            <div class="tab-pane fade active show" id="account">
                                <div class="row">
                                    <div class="col-sm-6 col-md-6 col-lg-3 pl-lg-0">
                                        <div class="iq-card iq-card-block iq-card-stretch iq-card-height">
                                            <div class="iq-card-body">
                                                <div class="d-flex align-items-center justify-content-between">
                                                    <h6>{{__('admin.Wallet')}}</h6>
                                                    <span class="iq-icon"><i class="ri-exchange-dollar-line"></i></span>
                                                </div>
                                                <div class="iq-customer-box d-flex align-items-center justify-content-between mt-3">
                                                    <div class="d-flex justify-content-center align-items-center">
                                                        <div class="rounded-circle iq-card-icon iq-bg-primary mr-2"> <i class="ri-exchange-dollar-line"></i></div>
                                                    </div>
                                                    <h2>${{$user->wallet->amount}}</h2>
                                                    <div class="iq-map text-primary font-size-32"><i class="ri-funds-line"></i></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-6 col-lg-3 pl-lg-0">
                                        <div class="iq-card iq-card-block iq-card-stretch iq-card-height">
                                            <div class="iq-card-body">
                                                <div class="d-flex align-items-center justify-content-between">
                                                    <h6>{{__('admin.Demo_Wallet')}}</h6>
                                                    <span class="iq-icon"><i class="ri-exchange-dollar-line"></i></span>
                                                </div>
                                                <div class="iq-customer-box d-flex align-items-center justify-content-between mt-3">
                                                    <div class="d-flex justify-content-center align-items-center">
                                                        <div class="rounded-circle iq-card-icon iq-bg-primary mr-2"> <i class="ri-exchange-dollar-line"></i></div>
                                                    </div>
                                                    <h2>${{$user->demo_wallet->amount}}</h2>
                                                    <div class="iq-map text-primary font-size-32"><i class="ri-funds-line"></i></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="personal-information" role="tabpanel">
                                <div class="iq-card">
                                    <div class="iq-card-header d-flex justify-content-between">
                                        <div class="iq-header-title">
                                            <h4 class="card-title">{{__('admin.User_Information')}}</h4>
                                        </div>
                                    </div>
                                    <div class="iq-card-body">
                                        <form method="POST" action="/admin/update-user">
                                            @csrf
                                            <input type="hidden" value="{{$user->id}}" name="user_id">
                                            <div class="row align-items-center">
                                                <div class="form-group col-sm-6">
                                                    <label for="fname">{{__('admin.First_name')}}:</label>
                                                    <input type="text" class="form-control" id="fname" value="{{$user->firstname}}" name="firstname">
                                                </div>
                                                <div class="form-group col-sm-6">
                                                    <label for="lname">{{__('admin.Last_name')}}:</label>
                                                    <input type="text" class="form-control" id="lname" value="{{$user->lastname}}" name="lastname">
                                                </div>
                                                <div class="form-group col-sm-6">
                                                    <label for="email">{{__('admin.Email')}}:</label>
                                                    <input type="text" class="form-control" id="email" value="{{$user->email}}" name="email">
                                                </div>
                                                <div class="form-group col-sm-6">
                                                    <label for="fone">{{__('admin.Phone')}}:</label>
                                                    <input type="text" class="form-control" id="fone" value="{{$user->phone}}" name="phone">
                                                </div>
                                                <div class="form-group col-sm-6">
                                                    <label for="dob">{{__('admin.Date_of_Birth')}}:</label>
                                                    <input type="text" class="form-control" id="dob" value="{{$user->date_of_birth}}" name="dob">
                                                </div>
                                                <div class="form-group col-sm-6">
                                                    <label for="coun">{{__('admin.Country')}}:</label>
                                                    <input type="text" class="form-control" id="coun" value="{{$user->country}}" name="country">
                                                </div>
                                                <div class="form-group col-sm-6">
                                                    <label for="state">{{__('admin.state')}}:</label>
                                                    <input type="text" class="form-control" id="state" value="{{$user->state}}" name="state">
                                                </div>
                                                <div class="form-group col-sm-6">
                                                    <label for="city">{{__('admin.city')}}:</label>
                                                    <input type="text" class="form-control" id="city" value="{{$user->city}}" name="state">
                                                </div>
                                                <div class="form-group col-sm-6">
                                                    <label for="address">{{__('admin.Address')}}:</label>
                                                    <textarea class="form-control" id="address" value="{{$user->address}}" rows="2" name="address">{{$user->address}}</textarea>
                                                </div>
                                                <div class="form-group col-sm-6">
                                                    <label for="address_two">{{__('admin.address2')}}:</label>
                                                    <textarea class="form-control" id="address_two" value="{{$user->address_two}}" rows="2" name="address_two">{{$user->address_two}}</textarea>
                                                </div>
                                                <div class="form-group col-sm-6">
                                                    <button type="submit" class="btn btn-primary px-5 py-2">{{__('admin.Update')}}</button>
                                                </div>

                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade col-12 px-0" id="verify" role="tabpanel">
                                <div class="row mx-0">
                                    @if($user->verify)
                                        @if($user->verify->identity_verification)
                                            <div class="col-sm-4">
                                                <div class="card iq-mb-3">
                                                    <img src="{{$user->verify->identity_verification}}" class="card-img-top" alt="#">
                                                    @if($user->verify->identity_status == 0)
                                                        <div class="card-body d-flex justify-content-between">
                                                            <form action="/admin/verify-identity" method="POST">
                                                                @csrf
                                                                <input type="hidden" name="user_id" value="{{$user->id}}">
                                                                <button type="submit" class="btn btn-primary btn-block py-2">{{__('admin.v_identity')}}</button>
                                                            </form>
                                                            <form action="/admin/reject-identity" method="POST">
                                                                @csrf
                                                                <input type="hidden" name="user_id" value="{{$user->id}}">
                                                                <button type="submit" class="btn btn-danger btn-block py-2">{{__('admin.Reject')}}</button>
                                                            </form>
                                                        </div>
                                                    @elseif($user->verify->identity_status == 1)
                                                        <h4 class="card-title text-center">ID Card <span class="badge badge-success" style="font-size: 10px">{{__('admin.Verified')}}</span> </h4>
                                                    @else
                                                        <h4 class="card-title text-center">ID Card <span class="badge badge-danger" style="font-size: 10px">{{__('admin.Reject')}}</span> </h4>
                                                    @endif


                                                </div>
                                            </div>
                                        @endif
                                        @if($user->verify->address_verification)
                                            <div class="col-sm-4">
                                            <div class="card iq-mb-3">
                                                <img src="{{$user->verify->address_verification}}" class="card-img-top" alt="#">
                                                @if($user->verify->address_status == 0)
                                                    <div class="card-body d-flex justify-content-between">
                                                        <form action="/admin/verify-address" method="POST">
                                                            @csrf
                                                            <input type="hidden" name="user_id" value="{{$user->id}}">
                                                            <button type="submit" class="btn btn-primary btn-block py-2">{{__('admin.Verify_Address')}}</button>
                                                        </form>
                                                        <form action="/admin/reject-address" method="POST">
                                                            @csrf
                                                            <input type="hidden" name="user_id" value="{{$user->id}}">
                                                            <button type="submit" class="btn btn-danger btn-block py-2">{{__('admin.Reject')}}</button>
                                                        </form>
                                                    </div>
                                                @elseif($user->verify->address_status == 1)
                                                    <h4 class="card-title text-center">Address <span class="badge badge-success" style="font-size: 10px">{{__('admin.Verified')}}</span> </h4>
                                                @else
                                                    <h4 class="card-title text-center">Address <span class="badge badge-danger" style="font-size: 10px">{{__('admin.Reject')}}</span> </h4>
                                                @endif
                                            </div>
                                        </div>
                                        @endif
                                        @if($user->verify->identity_verification == null && $user->verify->address_verification == null)
                                            <p class="alert alert-info col-12">{{__('admin.No_valid_ID_or_Address_uploaded')}}</p>
                                        @endif
                                    @else
                                        <p class="alert alert-info col-12">{{__('admin.No_valid_ID_or_Address_uploaded')}}</p>
                                    @endif

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal fade" id="credit-acc" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-modal="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalCenterTitle">{{__('admin.Credit_account')}}</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form action="/admin/credit-user-account" method="POST">
                                @csrf
                                <input type="hidden" name="user_id" value="{{$user->id}}">
                                <label for="amount">Amount</label>
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text px-3" style="border-bottom-left-radius:9px;border-top-left-radius:9px;border:1px solid #d7dbda;background-color: #fff;">$</span>
                                    </div>
                                    <input type="text" id="amount" class="form-control" placeholder="0.0" name="amount">
                                </div>
                                <div class="d-flex justify-content-end">
                                    <button type="submit" class="btn btn-primary px-3 py-2 border-0">{{__('admin.Credit_account')}}</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection