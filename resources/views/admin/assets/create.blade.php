@extends('inc.app')
@section('ass') active @endsection
@section('c_ass') active @endsection
@section('content')
    <div id="content-page" class="content-page">
        <div class="container-fluid">
            <div class="row justify-content-center">
                <div class="col-7">
                    <div class="iq-card">
                        <div class="iq-card-header d-flex justify-content-between">
                            <div class="iq-header-title">
                                <h4 class="card-title font-weight-bold">{{__('admin.create')}} {{$type->title}} {{__('admin.assets')}}</h4>
                            </div>
                        </div>
                        <div class="iq-card-body">
                            <form method="POST" action="/admin/assets/create-assets/{{$type_id}}">
                                @include('inc.notification')
                                @csrf
                                <div class="row mx-0">
                                    <div class="col-sm-12 pl-0">
                                        <div class="form-group asset">
                                            <label for="currency">{{__('admin.assets')}}:</label>
                                            <select class="form-control select-courses" id="exampleFormControlSelect1" name="asset" style="background-color:transparent">
                                                <option value="" selected disabled>{{__('admin.select')}} {{__('admin.assets')}}</option>
                                                @foreach($symbols as $symbol)
                                                    <option value="{{json_encode($symbol)}}">
                                                        {{
                                                            $symbol->symbol ?? $symbol->currency
                                                        }}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
{{--                                    <div class="col-sm-6 pl-0">--}}
{{--                                        <div class="form-group asset">--}}
{{--                                            <label for="quote">Quote asset:</label>--}}
{{--                                            <select class="form-control select-courses2" id="exampleFormControlSelect2" name="quote_asset" style="background-color:transparent">--}}
{{--                                                <option value="" selected disabled>Select asset</option>--}}
{{--                                                <option value="BTC">Bitcoin</option>--}}
{{--                                                <option value="ETH">Ethereum</option>--}}
{{--                                                <option value="LTC">Litecoin</option>--}}
{{--                                                <option value="USD">Dollar</option>--}}
{{--                                            </select>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
                                    @foreach($times as $time)
                                        <div class="col-sm-3 pl-0">
                                            <div class="input-group mb-3">
                                                <label for="per" class="col-12 px-0">{{__('admin.profit_per')}}  ({{$time->name}} {{__('admin.Seconds')}} ):</label>
                                                <input type="number" id="amount" class="form-control" name="{{$time->id}}" value="20">
                                                <div class="input-group-append">
                                                    <span class="input-group-text px-3" style="border-top-right-radius:9px;border-bottom-right-radius:9px;border:1px solid #d7dbda;background-color: #fff;">%</span>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                    <div class="col-12 pl-0">
                                        <button type="submit" class="btn btn-primary px-5 py-2 clickable">{{__('admin.Create_Assets')}}</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection