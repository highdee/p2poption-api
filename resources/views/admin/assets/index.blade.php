@extends('inc.app')
@section('assets') active @endsection
@section('content')
    <div id="content-page" class="content-page">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12 px-0">
                    @include('inc.notification')
                    <div class="iq-card">
                        <div class="iq-card-header d-flex justify-content-between align-items-center mb-2">
                            <div class="iq-header-title">
                                <h4 class="card-title">{{__('admin.assets')}}</h4>
                            </div>
                        </div>
                        <div class="row mb-4">
                            @foreach($types as $type)
                                <div class="iq-header col-md-2">
                                    <a href="/admin/assets/create-asset/{{$type->id}}" class="btn clickable credit py-2 px-3">
                                        @if($type->title == 'Forex')
                                            {{__('admin.Create_Forex_Assets')}}
                                        @else
                                            {{__('admin.create')}} {{$type->title}} {{__('admin.assets')}}
                                        @endif
                                    </a>
                                </div>
                            @endforeach
                        </div>
                        <div class="iq-card-body pt-0">
                            <div class="table-responsive">
                                <table id="datatable" class="table table-striped table-hover" >
                                    <thead>
                                        <tr>
                                            <th>{{__('admin.Base_asset')}}</th>
                                            <th>{{__('admin.Quote_asset')}}</th>
                                            <th>{{__('admin.Status')}}</th>
                                            <th>{{__('admin.Delete')}}</th>
                                            <th>{{__('admin.View')}}</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @foreach ($assets as $asset)
                                        <tr>
                                            <td>{{$asset->base_asset}}</td>
                                            <td>{{$asset->quote_asset}}</td>
                                            <td class="td-toggle">
                                                <form action="/admin/assets/toggle-assets-status" method="POST" id="Statustoggler{{$asset->id}}">
                                                    @csrf
                                                    <input type="hidden" name="asset_id" value="{{$asset->id}}">
                                                    <div class="toggle-btn input-group">
                                                        <label class="switch">
                                                            <input type="checkbox" {{$asset->status == 1? 'checked' : ''}} name="status" onclick="document.getElementById('Statustoggler{{$asset->id}}').submit()">
                                                            <span class="slider round"></span>
                                                        </label>
                                                    </div>
                                                </form>
                                            </td>
                                            <td>
                                                <button type="button" class="btn btn-danger clickable" data-toggle="modal" data-target="#delete{{$asset->id}}">
                                                    <i class="fa fa-trash mr-0"></i>
                                                </button>
                                            </td>
                                            <td><a class="btn clickable" href="/admin/assets/show-asset/{{$asset->id}}"><i class="fa fa-arrow-right mr-0" style="font-size: 17px"></i></a></td>
                                        </tr>
                                        <div class="modal fade show" id="delete{{$asset->id}}" tabindex="-1" role="dialog">
                                            <div class="modal-dialog modal-dialog-centered" role="document">
                                                <div class="modal-content pb-3">
                                                    <div class="modal-header border-0">
                                                        <h5 class="modal-title" id="exampleModalCenterTitle">{{__('admin.Delete')}} {{$asset->base_asset.'/'. $asset->quote_asset}}</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">×</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body text-center">
                                                        <i class="fa fa-trash text-danger" style="font-size: 90px"></i>
                                                        <h5 class="my-3">{{__('admin.are_you_sure')}}</h5>
                                                        <form action="/admin/assets/delete-asset" method="POST">
                                                            @csrf
                                                            <input type="hidden" name="asset_id" value="{{$asset->id}}">
                                                            <button class="btn btn-danger px-5 py-2 ">{{__('admin.Delete')}}</button>
                                                        </form>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th>{{__('admin.Base_asset')}}</th>
                                            <th>{{__('admin.Quote_asset')}}</th>
                                            <th>{{__('admin.Status')}}</th>
                                            <th>{{__('admin.Delete')}}</th>
                                            <th>{{__('admin.View')}}</th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection