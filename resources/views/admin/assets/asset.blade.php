@extends('inc.app')
@section('content')
    <div id="content-page" class="content-page">
        <div class="container-fluid">
            <div class="row justify-content-center">
                <div class="col-7">
                    <div class="iq-card">
                        <div class="iq-card-header d-flex justify-content-between">
                            <div class="iq-header-title">
                                <h4 class="card-title font-weight-bold">{{__('admin.Update')}} {{__('admin.assets')}}</h4>
                            </div>
                        </div>
                        <div class="iq-card-body">
                            <form method="POST" action="/admin/assets/update-asset">
                                @include('inc.notification')
                                @csrf
                                <input type="hidden" name="asset_id" value="{{$asset->id}}">
                                <div class="form-group asset">
                                    <label for="ba">{{__('admin.Base_asset')}}:</label>
                                    <input type="text" class="form-control" name="base_asset" value="{{$asset->base_asset}}" id="ba" disabled>
                                </div>
                                <div class="form-group asset">
                                    <label for="qa">{{__('admin.Quote_asset')}}:</label>
                                    <input id="qa" type="text" class="form-control" name="quote_asset" value="{{$asset->quote_asset}}" disabled>
                                </div>
                                @foreach($asset->timing as $time)
                                    <div class="input-group mb-3">
                                        <label for="percent" class="col-12 px-0">{{__('admin.percentage')}} {{__('admin.for')}} {{$time->name}} {{__('admin.Seconds')}}:</label>
                                        <input type="text" id="percent" class="form-control" name="{{$time->id}}" value="{{$time->pivot->percentage}}">
                                        <div class="input-group-append">
                                            <span class="input-group-text px-3" style="border-top-right-radius:9px;border-bottom-right-radius:9px;border:1px solid #d7dbda;background-color: #fff;">%</span>
                                        </div>
                                    </div>
                                @endforeach
                                <div class="toggle-btn input-group mb-3">
                                    <label for="status" class="col-12 px-0">{{__('admin.Status')}}:</label>
                                    <label class="switch">
                                        <input type="checkbox" {{$asset->status == 1? 'checked' : ''}} name="status">
                                        <span class="slider round"></span>
                                    </label>
                                </div>
                                <button type="submit" class="btn btn-primary px-5 py-2">{{__('admin.Update')}}</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection