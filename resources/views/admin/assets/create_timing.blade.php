@extends('inc.app')
@section('ass') active @endsection
@section('c_timing') active @endsection
@section('content')
    <div id="content-page" class="content-page">
        <div class="container-fluid">
            <div class="row justify-content-center">
                <div class="col-7">
                    <div class="iq-card">
                        <div class="iq-card-header d-flex justify-content-between">
                            <div class="iq-header-title">
                                <h4 class="card-title font-weight-bold">{{__('admin.Create_timing')}}</h4>
                            </div>
                        </div>
                        <div class="iq-card-body">
                            <form method="POST" action="/admin/assets/create-timings">
                                @include('inc.notification')
                                @csrf
                                <div class="row mx-0">
                                    <div class="col-sm-8 px-0">
                                        <div class="input-group mb-3">
                                            <label for="percent" class="col-12 px-0">{{__('admin.Time')}}:</label>
                                            <input type="text" id="percent" class="form-control" name="name">
                                            <div class="input-group-append">
                                                <span class="input-group-text px-3 text-lowercase" style="border-top-right-radius:9px;border-bottom-right-radius:9px;border:1px solid #d7dbda;background-color: #fff;">{{__('admin.Seconds')}}</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12 px-0">
                                        <button type="submit" class="btn btn-primary px-5 py-2 clickable">{{__('admin.Create_timing')}}</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection