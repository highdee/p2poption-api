@extends('inc.app')
@section('ass') active @endsection
@section('timing') active @endsection
@section('content')
    <div id="content-page" class="content-page">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12 px-0">
                    @include('inc.notification')
                    <div class="iq-card">
                        <div class="iq-card-header d-flex justify-content-between align-items-center">
                            <div class="iq-header-title">
                                <h4 class="card-title">{{__('admin.Timing')}}</h4>
                            </div>
                            <div class="iq-header-title-right">
                                <a href="/admin/assets/create-timing" class="btn clickable credit py-2 px-3">{{__('admin.Create_timing')}}</a>
                            </div>
                        </div>
                        <div class="iq-card-body pt-0">
                            <div class="table-responsive">
                                <table id="datatable" class="table table-striped table-hover" >
                                    <thead>
                                    <tr>
                                        <th>{{__('admin.Time')}}</th>
                                        <th>{{__('admin.Status')}}</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach ($times as $time)
                                        <tr>
                                            <td class="text-lowercase">{{$time->name}} {{__('admin.Seconds')}}</td>
                                            <td class="td-toggle">
                                                <form action="/admin/assets/toggle-timing" method="POST" id="Statustoggler{{$time->id}}">
                                                    @csrf
                                                    <input type="hidden" name="time_id" value="{{$time->id}}">
                                                    <div class="toggle-btn input-group">
                                                        <label class="switch">
                                                            <input type="checkbox" {{$time->status == 1? 'checked' : ''}} name="status" onclick="document.getElementById('Statustoggler{{$time->id}}').submit()">
                                                            <span class="slider round"></span>
                                                        </label>
                                                    </div>
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th>{{__('admin.Time')}}</th>
                                            <th>{{__('admin.Status')}}</th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection