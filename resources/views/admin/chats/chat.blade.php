@extends('inc.app')

@section('content')
    <div id="content-page" class="content-page">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12 px-0">
                    <div class="iq-card">
                        <div class="iq-card-body chat-page p-0">
                            <div class="chat-data-block">
                                <div class="row mx-0">
                                    <div class="col-12 chat-data p-0 chat-data-right">
                                        <div class="tab-content">
                                            <div class="tab-pane fade active show" id="default-block" role="tabpanel">
                                                <div class="chat-head">
                                                    <header class="d-flex justify-content-between align-items-center bg-white pt-3 pr-3 pl-3 pb-3">
                                                        <div class="d-flex align-items-center">
                                                            <div id="sidebar-toggle" class="sidebar-toggle">
                                                                <i class="ri-menu-3-line"></i>
                                                            </div>
                                                            <div class="avatar chat-user-profile m-0 mr-3">
                                                                <img src="/assets/images/user/a.png" alt="avatar" class="avatar-50 ">
                                                                <span class="avatar-status"><i class="ri-checkbox-blank-circle-fill text-success"></i></span>
                                                            </div>
                                                            <h5 class="mb-0">{{$user->firstname}} {{$user->lastname}}</h5>
                                                        </div>
                                                        <div class="chat-user-detail-popup scroller">
                                                            <div class="user-profile text-center">
                                                                <button type="submit" class="close-popup p-3"><i class="ri-close-fill"></i></button>
                                                                <div class="user mb-4">
                                                                    <a class="avatar m-0">
                                                                        <img src="/assets/images/user/a.png" alt="avatar">
                                                                    </a>
                                                                    <div class="user-name mt-4"><h4>{{$user->firstname}} {{$user->lastname}}</h4></div>
                                                                    <div class="user-desc"><p>{{$user->email}}</p></div>
                                                                </div>
                                                                <hr>
                                                                <div class="chatuser-detail text-left mt-4">
                                                                    <div class="row">
                                                                        <div class="col-6 col-md-6 title">{{__('admin.Address')}}:</div>
                                                                        <div class="col-6 col-md-6 text-right">{{$user->address}}</div>
                                                                    </div><hr>
                                                                    <div class="row">
                                                                        <div class="col-6 col-md-6 title">{{__('admin.tel')}}:</div>
                                                                        <div class="col-6 col-md-6 text-right">{{$user->phone}}</div>
                                                                    </div><hr>
                                                                    <div class="row">
                                                                        <div class="col-6 col-md-6 title">{{__('admin.gender')}}:</div>
                                                                        <div class="col-6 col-md-6 text-right">{{$user->gender}}</div>
                                                                    </div><hr>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </header>
                                                </div>
                                                <div class="chat-content scroller">
                                                    @foreach ($chats as $msg)
                                                        @if ($msg->sender != 0)
                                                            <div class="chat">
                                                                <div class="chat-user">
                                                                    <a class="avatar m-0">
                                                                        <img src="{{$user->profile_image?$user->profile_image:'/assets/images/user/a.png'}}" alt="avatar" class="avatar-35 ">
                                                                    </a>
                                                                    <span class="chat-time mt-1">{{$msg->created_at->format('D, d M Y')}}</span>
                                                                </div>
                                                                <div class="chat-detail">
                                                                    <div class="chat-message">
                                                                        <p>{{$msg->message}}</p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        @else
                                                            <div class="chat chat-left">
                                                                <div class="chat-user">
                                                                    <a class="avatar m-0">
                                                                        <img src="/assets/images/user/a.png" alt="avatar" class="avatar-35 ">
                                                                    </a>
                                                                    <span class="chat-time mt-1">{{$msg->created_at->format('D, d M Y')}}</span>
                                                                </div>
                                                                <div class="chat-detail">
                                                                    <div class="chat-message">
                                                                        <p>{{$msg->message}}</p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        @endif
                                                    @endforeach


                                                </div>
                                                <div class="chat-footer p-3 bg-white">
                                                    <form class="d-flex align-items-center"  action="/admin/chats/send-message" method="POST">
                                                        @csrf
                                                        <div class="chat-attagement d-flex">
                                                            <a href="javascript:void();"><i class="fa fa-paperclip pr-3" aria-hidden="true"></i></a>
                                                        </div>
                                                        <input type="hidden" name="receiver" value="{{$user->id}}" id="">
                                                        <input type="text" name="message" class="form-control mr-3" placeholder="{{__('admin.Type_your_message')}}">
                                                        <button type="submit" class="btn btn-primary d-flex align-items-center p-2" style="width: 100px">
                                                            <i class="fa fa-paper-plane-o" aria-hidden="true"></i>
                                                            <span class="d-none d-lg-block ml-1">{{__('admin.send')}}</span>
                                                        </button>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
