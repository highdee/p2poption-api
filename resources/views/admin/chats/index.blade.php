@extends('inc.app')
@section('chats')
    active
@endsection
@section('content')
    <div id="content-page" class="content-page">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12 px-0">
                    <div class="iq-card">
                        <div class="iq-card-header d-flex justify-content-between align-items-center">
                            <div class="iq-header-title">
                                <h4 class="card-title">{{__('admin.chats')}}</h4>
                            </div>
                        </div>
                        <div class="iq-card-body pt-0">
                            <div class="table-responsive">
                                <table id="datatable" class="table table-striped table-hover" >
                                    <thead>
                                        <tr>
                                            <th>{{__('admin.name')}}</th>
                                            <th>{{__('admin.Time')}}</th>
                                            <th>{{__('admin.message')}}</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($chats as $chat)
                                            <tr onclick="window.location.href='/admin/chats/{{$chat->user?$chat->user->id:0}}'">
                                                <td>{{$chat->user?$chat->user->firstname .' '.$chat->user->lastname:''}}</td>
                                                <td>{{$chat->created_at->format('D, d M Y')}}</td>
                                                <td>{{$chat->message}}</td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th>{{__('admin.name')}}</th>
                                            <th>{{__('admin.Time')}}</th>
                                            <th>{{__('admin.message')}}</th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection