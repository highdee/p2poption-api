@extends('inc.app')
@section('withdrawal')
    active
@endsection
@section('content')
    <div id="content-page" class="content-page">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12 px-0">
                    <div class="iq-card">
                        <div class="iq-card-header d-flex justify-content-between align-items-center">
                            <div class="iq-header-title">
                                <h4 class="card-title">{{__('admin.withdrawal')}}</h4>
                            </div>
                        </div>
                        <div class="iq-card-body pt-0">
                            <div class="table-responsive">
                                <table id="datatable" class="table table-striped table-hover" >
                                    <thead>
                                        <tr>
                                            <th>{{__('admin.Users')}}</th>
                                            <th>{{__('admin.Amount')}}</th>
                                            <th>{{__('admin.Address')}}</th>
                                            <th>{{__('admin.Created')}}</th>
                                            <th>{{__('admin.Status')}}</th>
                                            <th>{{__('admin.Actions')}}</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @foreach ($withdrawals as $withdraw)
                                        <tr onclick="">
                                            <td>
                                                @if($withdraw->user)
                                                    <span>{{$withdraw->user->firstname}} {{$withdraw->user->lastname}}</span>
                                                @else
                                                    <span>N?A</span>
                                                @endif
                                            </td>
                                            <td>{{$withdraw->amount}}</td>
                                            <td>{{$withdraw->address}}</td>
                                            <td>{{$withdraw->created_at}}</td>
                                            <td>
                                                @if($withdraw->accepted_at)
                                                    <span class="text-success font-weight-bold border border-success iq-border-radius-10 px-4 py-2">{{__('admin.accepted')}}</span>
                                                @elseif($withdraw->rejected_at)
                                                    <span class="text-danger font-weight-bold border border-danger iq-border-radius-10 px-4 py-2">{{__('admin.Reject')}}</span>
                                                @else
                                                    <span class="text-warning font-weight-bold border border-warning iq-border-radius-10 px-4 py-2">{{__('admin.pending')}}</span>
                                                @endif
                                            </td>
                                            <td class="d-flex justify-content-lg-start align-items-center">
                                                @if($withdraw->accepted_at == null)
                                                    <form method="post" action="/admin/withdrawal/action/1" class="mr-3">
                                                        @csrf
                                                        <input type="hidden" name="id" value="{{ $withdraw->id }}">
                                                        <button type="submit" class="btn btn-success">{{__('admin.accept')}}</button>
                                                    </form>
                                                    <form method="post" action="/admin/withdrawal/action/0" >
                                                        @csrf
                                                        <input type="hidden" name="id" value="{{ $withdraw->id }}">
                                                        <button type="submit" class="btn btn-danger">{{__('admin.Reject')}}</button>
                                                    </form>
                                                @endif

                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th>{{__('admin.Users')}}</th>
                                            <th>{{__('admin.Amount')}}</th>
                                            <th>{{__('admin.Address')}}</th>
                                            <th>{{__('admin.Created')}}</th>
                                            <th>{{__('admin.Status')}}</th>
                                            <th>{{__('admin.Actions')}}</th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection