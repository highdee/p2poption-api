@extends('inc.app')
@section('content')
    <div id="content-page" class="content-page">
        <div class="container-fluid">
            <div class="row justify-content-center">
                <div class="col-7">
                    <div class="iq-card">
                        <div class="iq-card-header d-flex justify-content-between">
                            <div class="iq-header-title">
                                <h4 class="card-title font-weight-bold">{{__('admin.edit')}} {{__('admin.exchange_rate_extra')}}</h4>
                            </div>
                        </div>
                        <div class="iq-card-body">
                            <form method="POST" action="/admin/update-exchanges-rate">
                                @include('inc.notification')
                                @csrf
                                <input type="hidden" name="rate_id" id="" value="{{$rate->id}}">
                                <div class="form-group">
                                    <label for="currency">{{__('admin.Currency')}}:</label>
                                    <input disabled type="text" class="form-control" id="currency" name="currency" value="{{$rate->currency}}">
                                </div>
                                <div class="form-group">
                                    <label for="currency">Current Price:</label>
                                    <input disabled type="text" class="form-control"  value="{{$current_price}}">
                                </div>
                                <div class="form-group">
                                    <label for="rate">Percentage %:</label>
                                    <input type="text" class="form-control" id="rate" name="rate" value="{{$rate->percentage}}">
                                </div>
                                <button type="submit" class="btn btn-primary px-5 py-2">{{__('admin.Update')}}</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection