@extends('inc.app')
@section('content')
    <div id="content-page" class="content-page">
        <div class="container-fluid px-3">
            <div class="student-tabs pt-3 pb-4 d-flex justify-content-between">
                <ul class="d-flex nav nav-pills">
                    <li>
                        <a class="nav-link active" data-toggle="pill" href="#details">
                            {{__('admin.Detail')}}
                        </a>
                    </li>
                    <li>
                        <a class="nav-link" data-toggle="pill" href="#personal-information">
                            {{__('admin.User_Information')}}
                        </a>
                    </li>
                    <li>
                        <a class="nav-link" data-toggle="pill" href="#links">
                            {{__('admin.Affiliates_link')}}
                        </a>
                    </li>
                    <li>
                        <a class="nav-link" data-toggle="pill" href="#rc">
                            {{__('admin.Referral')}}
                        </a>
                    </li>
                </ul>
                <ul class="d-flex">
                    <li class="mr-2">
                        <form action="/admin/affiliates/ban-affiliate" method="POST">
                            @csrf
                            <input type="hidden" name="affiliate_id" value="{{$aff->id}}">
                            <button type="submit" class="btn clickable {{$aff->status == -1?'btn-danger':'btn-success'}}" >{{$aff->status==-1?__('admin.inactive'):__('admin.active')}}</button>
                        </form>
                    </li>
                    <li>
                        <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#delete-aff"><i class="fa fa-trash mr-0 fa-lg"></i></button>
                    </li>
                </ul>
            </div>

            <div class="row">
                <div class="col-lg-12 px-0">
                    <div class="iq-edit-list-data">
                        <div class="tab-content">
                            @include('inc.notification')
                            <div class="tab-pane fade active show" id="details">
                                <div class="row">
                                    <div class="col-sm-6 col-md-6 col-lg-3">
                                        <div class="iq-card iq-card-block iq-card-stretch iq-card-height">
                                            <div class="iq-card-body">
                                                <div class="d-flex align-items-center justify-content-between">
                                                    <h6>{{__('admin.Affiliates_link')}}</h6>
                                                    <span class="iq-icon"><i class="ri-information-fill"></i></span>
                                                </div>
                                                <div class="iq-customer-box d-flex align-items-center justify-content-between mt-3">
                                                    <div class="d-flex justify-content-center align-items-center">
                                                        <div class="rounded-circle iq-card-icon iq-bg-primary mr-2"> <i class="ri-price-tag-3-line"></i></div>
                                                    </div>
                                                    <h2>{{count($aff->referrals)}}</h2>
                                                    <div class="iq-map text-primary font-size-32"><i class="fa fa-link"></i></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-6 col-lg-3">
                                        <div class="iq-card iq-card-block iq-card-stretch iq-card-height">
                                            <div class="iq-card-body">
                                                <div class="d-flex align-items-center justify-content-between">
                                                    <h6>{{__('admin.Registered_user')}}</h6>
                                                    <span class="iq-icon"><i class="fa fa-user"></i></span>
                                                </div>
                                                <div class="iq-customer-box d-flex align-items-center justify-content-between mt-3">
                                                    <div class="d-flex justify-content-center align-items-center">
                                                        <div class="rounded-circle iq-card-icon iq-bg-primary mr-2"> <i class="fa fa-users"></i></div>
                                                    </div>
                                                    <h2>{{$aff['count_referral']}}</h2>
                                                    <div class="iq-map text-primary font-size-32"><i class="fa fa-user"></i></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-6 col-lg-3">
                                        <div class="iq-card iq-card-block iq-card-stretch iq-card-height">
                                            <div class="iq-card-body">
                                                <div class="d-flex align-items-center justify-content-between">
                                                    <h6>{{__('admin.Wallet')}}</h6>
                                                    <span class="iq-icon"><i class="ri-exchange-dollar-line"></i></span>
                                                </div>
                                                <div class="iq-customer-box d-flex align-items-center justify-content-between mt-3">
                                                    <div class="d-flex justify-content-center align-items-center">
                                                        <div class="rounded-circle iq-card-icon iq-bg-primary mr-2"> <i class="ri-exchange-dollar-line"></i></div>
                                                    </div>
                                                    <h2>${{$aff->wallet->amount}}</h2>
                                                    <div class="iq-map text-primary font-size-32"><i class="ri-funds-line"></i></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-6 col-lg-3">
                                        <div class="iq-card iq-card-block iq-card-stretch iq-card-height">
                                            <div class="iq-card-body">
                                                <div class="d-flex align-items-center justify-content-between">
                                                    <h6>{{__('admin.visitors')}}</h6>
                                                    <span class="iq-icon"><i class="fa fa-street-view"></i></span>
                                                </div>
                                                <div class="iq-customer-box d-flex align-items-center justify-content-between mt-3">
                                                    <div class="d-flex justify-content-center align-items-center">
                                                        <div class="rounded-circle iq-card-icon iq-bg-primary mr-2"> <i class="ri-inbox-fill"></i></div>
                                                    </div>
                                                    <h2>{{$aff->visitors}}</h2>
                                                    <div class="iq-map text-primary font-size-32"><i class="fa fa-street-view"></i></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="personal-information" role="tabpanel">
                                <div class="iq-card">
                                    <div class="iq-card-header d-flex justify-content-between">
                                        <div class="iq-header-title">
                                            <h4 class="card-title">{{__('admin.User_Information')}}</h4>
                                        </div>
                                    </div>
                                    <div class="iq-card-body">
                                        <form method="POST" action="/admin/affiliates/update-affiliate">
                                            @csrf
                                            <input type="hidden" value="{{$aff->id}}" name="affiliate_id">
                                            <div class="row align-items-center">
                                                <div class="form-group col-sm-6">
                                                    <label for="fname">{{__('admin.First_name')}}:</label>
                                                    <input type="text" class="form-control" id="fname" value="{{$aff->firstname}}" name="firstname">
                                                </div>
                                                <div class="form-group col-sm-6">
                                                    <label for="lname">{{__('admin.Last_name')}}:</label>
                                                    <input type="text" class="form-control" id="lname" value="{{$aff->lastname}}" name="lastname">
                                                </div>
                                                <div class="form-group col-sm-6">
                                                    <label for="usr">{{__('admin.username')}}:</label>
                                                    <input type="text" class="form-control" id="usr" value="{{$aff->username}}" name="username">
                                                </div>
                                                <div class="form-group col-sm-6">
                                                    <label for="email">{{__('admin.visitors')}}:</label>
                                                    <input type="text" class="form-control" id="email" value="{{$aff->email}}" name="email">
                                                </div>
                                                <div class="form-group col-sm-6">
                                                    <label for="fone">{{__('admin.Phone')}}:</label>
                                                    <input type="text" class="form-control" id="fone" value="{{$aff->phone}}" name="phone">
                                                </div>
                                                <div class="form-group col-sm-6">
                                                    <label for="coun">{{__('admin.Country')}}:</label>
                                                    <input type="text" class="form-control" id="coun" value="{{$aff->country}}" name="country">
                                                </div>
                                                <div class="form-group col-sm-6">
                                                    <button type="submit" class="btn btn-primary px-5 py-2">{{__('admin.Update')}}</button>
                                                </div>

                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>

                            <div class="tab-pane fade col-12 px-0" id="links" role="tabpanel">
                                <div class="iq-card">
                                    <div class="iq-card-body">
                                        <div class="iq-card-body pt-0">
                                            <div class="table-responsive">
                                                <table id="datatable" class="table table-striped table-hover" >
                                                    <thead>
                                                        <tr>
                                                            <th>{{__('admin.Referral_Id')}}</th>
                                                            <th>{{__('admin.Referral_link')}}</th>
                                                            <th>{{__('admin.Offer_type')}}</th>
                                                            <th>{{__('admin.Promo_code')}}</th>
                                                            <th>{{__('admin.Registered_user')}}</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @foreach ($aff->referrals as $ref)
                                                            <tr>
                                                                <td>{{$ref->token}}</td>
                                                                <td>{{$ref->link}}</td>
                                                                <td>{{$ref->offer_type}}</td>
                                                                <td>{{$ref->promocode}}</td>
                                                                <td>{{count($ref->referral_users)}}</td>
                                                            </tr>
                                                        @endforeach
                                                    </tbody>
                                                    <tfoot>
                                                        <tr>
                                                            <th>{{__('admin.Referral_Id')}}</th>
                                                            <th>{{__('admin.Referral_link')}}</th>
                                                            <th>{{__('admin.Offer_type')}}</th>
                                                            <th>{{__('admin.Promo_code')}}</th>
                                                            <th>{{__('admin.Registered_user')}}</th>
                                                        </tr>
                                                    </tfoot>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade col-12 px-0" id="rc" role="tabpanel">
                                <div class="iq-card">
                                    <div class="iq-card-body">
                                        <div class="iq-card-body pt-0">
                                            <div class="table-responsive">
                                                <table id="referral_table" class="table table-striped table-hover">
                                                    <thead>
                                                        <tr>
                                                            <th>{{__('admin.name')}}</th>
                                                            <th>{{__('admin.Email')}}</th>
                                                            <th>{{__('admin.Referral_link')}}</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @foreach ($aff->users as $user)
                                                            <tr>
                                                                <td>{{$user->firstname}} {{$user->lastname}}</td>
                                                                <td>{{$user->email}}</td>
                                                                <td>{{count($user->referral) < 1 ?'':$user->referral[0]->link}}</td>
                                                            </tr>
                                                        @endforeach
                                                    </tbody>
                                                    <tfoot>
                                                        <tr>
                                                            <th>{{__('admin.name')}}</th>
                                                            <th>{{__('admin.email')}}</th>
                                                            <th>{{__('admin.Referral_link')}}</th>
                                                        </tr>
                                                    </tfoot>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade show" id="delete-aff" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content pb-3">
                <div class="modal-header border-0">
                    <h5 class="modal-title" id="exampleModalCenterTitle"><th>{{__('admin.Delete')}}</th> {{$aff->firstname}} {{$aff->lastname}}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body text-center">
                    <i class="fa fa-trash text-danger" style="font-size: 90px"></i>
                    <h5 class="my-3"><th>{{__('admin.are_you_sure')}}</th></h5>
                    <form action="/admin/affiliates/delete-affiliate" method="POST">
                        @csrf
                        <input type="hidden" name="affiliate_id" value="{{$aff->id}}">
                        <button type="submit" class="btn btn-danger px-5 py-2"><th>{{__('admin.Delete')}}</th></button>
                    </form>

                </div>
            </div>
        </div>
    </div>
@endsection