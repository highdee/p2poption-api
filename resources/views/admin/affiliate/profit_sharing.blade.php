@extends('inc.app')
@section('affs') active @endsection
@section('profit_s') active @endsection
@section('content')
    <div id="content-page" class="content-page">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12 px-0">
                    @include('inc.notification')
                    <div class="iq-card">
                        <div class="iq-card-header d-flex justify-content-between align-items-center">
                            <div class="iq-header-title">
                                <h4 class="card-title">{{__('admin.profit_s_c')}}</h4>
                            </div>
                            <div class="iq-header-title-right">
                                <button type="button" class="btn clickable credit py-2 px-3" data-toggle="modal" data-target="#create_ps">
                                    {{__('admin.c_ps')}}
                                </button>
                            </div>
                            <div class="modal fade" id="create_ps" tabindex="-1" role="dialog" aria-labelledby="create_ps" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="create_ps">{{__('admin.c_ps')}}</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">×</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <form method="POST" action="/admin/affiliates/add-profit-sharing">
                                                @csrf
                                                <div class="form-group">
                                                    <label for="name">{{__('admin.name')}}:</label>
                                                    <input type="text" class="form-control" id="name" name="name">
                                                </div>
                                                <div class="form-group">
                                                    <label for="percent">{{__('admin.percentage')}}(%):</label>
                                                    <input type="text" class="form-control" id="percent" name="percentage">
                                                </div>
                                                <div class="form-group">
                                                    <button type="submit" class="btn btn-primary px-5 py-2 clickable">{{__('admin.create')}}</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="iq-card-body pt-0">
                            <div class="table-responsive">
                                <table id="datatable" class="table table-striped table-hover" >
                                    <thead>
                                        <tr>
                                            <th>{{__('admin.name')}}</th>
                                            <th>{{__('admin.percentage')}}</th>
                                            <th>{{__('admin.edit')}}</th>
                                            <th>{{__('admin.Delete')}}</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($pss as $ps)
                                            <tr>
                                                <td>{{$ps->name}}</td>
                                                <td>{{$ps->percentage}}</td>
                                                <td>
                                                    {{-- <button type="button" class="btn clickable credit py-2 px-3" data-toggle="modal" data-target="#edit{{$ps->id}}"> --}}
                                                        <i class="fa fa-edit font-size-5" data-toggle="modal" data-target="#edit{{$ps->id}}"></i>
                                                    {{-- </button> --}}
                                                </td>
                                                <td>
                                                    <i class="fa fa-trash font-size-5 text-danger" data-toggle="modal" data-target="#delete{{$ps->id}}"></i>
                                                </td>
                                            </tr>
                                            <div class="modal fade" id="edit{{$ps->id}}" tabindex="-1" role="dialog" aria-labelledby="edit{{$ps->id}}" aria-hidden="true">
                                                <div class="modal-dialog modal-dialog-centered" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title" id="edit{{$ps->id}}">{{__('admin.edit')}} {{__('admin.c_ps')}}</h5>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">×</span>
                                                            </button>
                                                        </div>
                                                        <div class="modal-body">
                                                            <form method="POST" action="/admin/affiliates/update-profit-sharing">
                                                                @csrf
                                                                <input type="hidden" name="share_id" value="{{$ps->id}}">
                                                                <div class="form-group">
                                                                    <label for="name">{{__('admin.name')}}:</label>
                                                                    <input type="text" class="form-control" id="name" name="name" value="{{$ps->name}}">
                                                                </div>
                                                                <div class="form-group">
                                                                    <label for="percent">{{__('admin.percentage')}} (%):</label>
                                                                    <input type="text" class="form-control" id="percent" name="percentage" value="{{$ps->percentage}}">
                                                                </div>
                                                                <div class="form-group">
                                                                    <button type="submit" class="btn btn-primary px-5 py-2 clickable">{{__('admin.Update')}}</button>
                                                                </div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="modal fade" id="delete{{$ps->id}}" tabindex="-1" role="dialog" aria-labelledby="delete{{$ps->id}}" aria-hidden="true">
                                                <div class="modal-dialog modal-dialog-centered" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title" id="edit{{$ps->id}}">{{__('admin.Delete')}} {{$ps->name}}</h5>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">×</span>
                                                            </button>
                                                        </div>
                                                        <div class="modal-body">
                                                            <div class="text-center">
                                                                <i class="fa fa-trash text-danger" style="font-size:90px"></i>
                                                                <h5>{{__('admin.are_you_sure')}}</h5>
                                                            </div>
                                                            <form method="POST" action="/admin/affiliates/delete-profit-sharing">
                                                                @csrf
                                                                <input type="hidden" name="share_id" value="{{$ps->id}}">
                                                                <div class="form-group text-center mt-4">
                                                                    <button type="submit" class="btn btn-danger px-5 py-2 clickable">{{__('admin.Delete')}}</button>
                                                                </div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th>{{__('admin.name')}}</th>
                                            <th>{{__('admin.percentage')}}</th>
                                            <th>{{__('admin.edit')}}</th>
                                            <th>{{__('admin.Delete')}}</th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection