@extends('inc.app')
@section('content')
    <div id="content-page" class="content-page">
        <div class="container-fluid">
            <div class="row justify-content-center">
                <div class="col-7">
                    <div class="iq-card">
                        <div class="iq-card-header d-flex justify-content-between">
                            <div class="iq-header-title">
                                <h4 class="card-title font-weight-bold">Credit User Account</h4>
                            </div>
                        </div>
                        <div class="iq-card-body">
                            <form method="post" action="/admin/verify-credit-token">
                                @include('inc.notification')
                                @csrf
                                <input type="hidden" name="user_id" value="{{$user_id}}">
                                <div class="form-group">
                                    <label for="user">User:</label>
                                    <input type="text" class="form-control" id="user" name="user" placeholder="{{$user}}">
                                </div>
                                <div class="form-group">
                                    <label for="fname">Amount:</label>
                                    <input type="text" class="form-control" name="amount" value="${{$amount}}" disabled>
                                </div>
                                <div class="form-group">
                                    <label for="">Token:</label>
                                    <input type="text" class="form-control" name="token">
                                </div>
                                <button type="submit" class="btn btn-primary px-5 py-2">Submit</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection