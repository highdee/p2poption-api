<!doctype html>
<html lang="en">
   <head>
      <!-- Required meta tags -->
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <title></title>
      <link rel="stylesheet" href="{{asset('assets/css/bootstrap.min.css')}}">
      <link rel="stylesheet" href="{{asset('assets/css/typography.css')}}">
      <link rel="stylesheet" href="{{asset('assets/css/style.css')}}">
      <link rel="stylesheet" href="{{asset('assets/css/responsive.css')}}">
      <link rel="stylesheet" href="{{asset('assets/css/main.css')}}">
      <link rel="stylesheet" href="{{asset('assets/css/select2.min.css')}}">
      <link href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap4.min.css" rel="stylesheet">
   </head>
   <body class="sidebar-main-active right-column-fixed header-top-bgcolor">
        <div class="wrapper">
         <!-- Sidebar  -->
            <div class="iq-sidebar">
                <div class="iq-sidebar-logo d-flex justify-content-between pl-0">
                    <a href="/admin" >
                        <span>P2P {{__('admin.Admin')}}</span>
                    </a>
                    <div class="iq-menu-bt-sidebar">
                        <div class="iq-menu-bt align-self-center">
                            <div class="wrapper-menu">
                                <div class="main-circle"><i class="ri-arrow-left-s-line"></i></div>
                                <div class="hover-circle"><i class="ri-arrow-right-s-line"></i></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="sidebar-scrollbar">
                    <nav class="iq-sidebar-menu">
                        <ul id="iq-sidebar-toggle" class="iq-menu">
                            <li class="@yield('dashboard')"><a class="iq-waves-effect" href="/admin"><i class="ri-home-4-line"></i><span>{{__('admin.dashboard')}}</span></a></li>
                            <li><a class="iq-waves-effect @yield('users')" href="/admin/users"><i class="ri-user-line"></i><span>{{__('admin.Users')}}</span></a></li>
                            <li><a class="iq-waves-effect @yield('exchange-rates')" href="/admin/exchange-rates"><i class="ri-exchange-dollar-fill"></i><span>{{__('admin.extra_ex')}}</span></a></li>
                            <li class="@yield('deposits')">
                                <a href="#deposits" class="iq-waves-effect collapsed" data-toggle="collapse" aria-expanded="false"><i class="fa fa-handshake-o"></i><span>{{__('admin.deposits')}}</span><i class="ri-arrow-right-s-line iq-arrow-right"></i></a>
                                <ul id="deposits" class="iq-submenu iq-submenu-data collapse" data-parent="#iq-sidebar-toggle">
                                    <li class="@yield('dep')"><a href="/admin/deposits/"><i class="ri-record-circle-line"></i>{{__('admin.deposits')}}</a></li>
                                    <li class="@yield('currencies')"><a href="/admin/deposit-currencies/"><i class="ri-record-circle-line"></i>{{__('admin.deposits')}} {{__('admin.Currency')}}</a></li>
                                </ul>
                            </li>
{{--                            <li><a class="iq-waves-effect @yield('deposits')" href="/admin/deposits"><i class="fa fa-dot-circle-o"></i><span>Deposit</span></a></li>--}}
                            {{-- <li class="@yield('bonus_percent')"><a class="iq-waves-effect" href="/admin/deposit-bonus"><i class="fa fa-gift"></i><span>Deposit bonus</span></a></li> --}}
                            <li class="@yield('withdrawal')"><a class="iq-waves-effect" href="/admin/withdrawal"><i class="fa fa-credit-card"></i><span>{{__('admin.withdrawal')}}</span></a></li>
                            <li class="@yield('assets')"><a class="iq-waves-effect" href="/admin/assets/"><i class="fa fa-gift"></i><span>{{__('admin.assets')}}</span></a></li>
                            <li class="@yield('c_timing')"><a class="iq-waves-effect" href="/admin/assets/timings"><i class="fa fa-hourglass"></i></i><span>{{__('admin.Timing')}}</span></a></li>
                            <li class="@yield('aff')">
                                <a href="#affiliate" class="iq-waves-effect collapsed" data-toggle="collapse" aria-expanded="false"><i class="fa fa-link"></i><span>{{__('admin.Affiliate')}}</span><i class="ri-arrow-right-s-line iq-arrow-right"></i></a>
                                <ul id="affiliate" class="iq-submenu iq-submenu-data collapse" data-parent="#iq-sidebar-toggle">
                                    <li class="@yield('affs')"><a href="/admin/affiliates/"><i class="ri-record-circle-line"></i>{{__('admin.Affiliate')}}</a></li>
                                    <li class="@yield('aff_percent')"><a href="/admin/affiliates/affiliate-percentage/"><i class="ri-record-circle-line"></i>{{__('admin.bonus_per')}}</a></li>
{{--                                    <li class="@yield('profit_s')"><a href="/admin/affiliates/profit-sharing/"><i class="ri-record-circle-line"></i>{{__('admin.profit_s_c')}}</a></li>--}}
                                </ul>
                            </li>
                            <li class="@yield('verify-request')"><a class="iq-waves-effect" href="/admin/verification-request"><i class="fa fa-question-circle"></i><span>{{__('admin.verification_request')}}</span></a></li>
                            <li class="@yield('chats')"><a class="iq-waves-effect" href="/admin/chats"><i class="fa fa-comment"></i><span>{{__('admin.chats')}}</span></a></li>
                        </ul>
                    </nav>
                    <div class="p-3"></div>
                </div>
            </div>
            
            @include('inc.navbar')
            @yield('content')
            
        </div>
        @include('inc.footer')
      <!-- jQuery first, then Popper.js, then Bootstrap JS -->
      <script src="{{asset('assets/js/jquery.min.js')}}"></script>
      <script src="{{asset('assets/js/popper.min.js')}}"></script>
      <script src="{{asset('assets/js/bootstrap.min.js')}}"></script>
      {{-- <script src="{{asset('assets/js/jquery.appear.js')}}"></script> --}}
      {{-- <script src="{{asset('assets/js/countdown.min.js')}}"></script> --}}
      {{-- <script src="{{asset('assets/js/waypoints.min.js')}}"></script> --}}
      <script src="{{asset('assets/js/jquery.counterup.min.js')}}"></script>
      <script src="{{asset('assets/js/wow.min.js')}}"></script>
      {{-- <script src="{{asset('assets/js/apexcharts.js')}}"></script> --}}
      <script src="{{asset('assets/js/slick.min.js')}}"></script>
      <script src="{{asset('assets/js/select2.min.js')}}"></script>
      <script src="{{asset('assets/js/owl.carousel.min.js')}}"></script>
      <script src="{{asset('assets/js/jquery.magnific-popup.min.js')}}"></script>
      <script src="{{asset('assets/js/smooth-scrollbar.js')}}"></script>
      <script src="{{asset('assets/js/lottie.js')}}"></script>
      <script src="{{asset('assets/js/core.js')}}"></script>
      {{-- <script src="{{asset('assets/js/charts.js')}}"></script> --}}
      <script src="{{asset('assets/js/animated.js')}}"></script>
      {{-- <script src="{{asset('assets/js/kelly.js')}}"></script> --}}
      {{-- <script src="{{asset('assets/js/maps.js')}}"></script> --}}
      {{-- <script src="{{asset('assets/js/worldLow.js')}}"></script> --}}
      <script src="{{asset('assets/js/chart-custom.js')}}"></script>
      <script src="{{asset('assets/js/custom.js')}}"></script>

      <script src="{{asset('assets/js/main.js')}}"></script>
      <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
      <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
      @yield('script')

   </body>
</html>
