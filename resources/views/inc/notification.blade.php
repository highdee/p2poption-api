{{-- <div class="alert text-white bg-danger" role="alert">
    <div class="iq-alert-text">A simple <b>danger</b> alert—check it out!</div>
 </div> --}}
 @if (session()->has('msg'))
    @if(session()->get('status'))
        <div class="alert alert-success text-center">
            <ul class="list-unstyled my-0 py-0">
                <li>{{ session()->get('msg') }}</li>
            </ul>
        </div>
    @else
        <div class="alert alert-danger  text-center">
            <ul class="list-unstyled my-0 py-0">
                <li>{{ session()->get('msg') }}</li>
            </ul>
        </div>
    @endif
@endif
@if ($errors->any())
    <div class="alert alert-danger py-1">
        <ul class="list-unstyled py-0 my-0">
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
