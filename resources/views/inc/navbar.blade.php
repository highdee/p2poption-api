<!-- TOP Nav Bar -->
<div class="iq-top-navbar mr-0">
    <div class="iq-navbar-custom">
        <div class="iq-sidebar-logo">
            <div class="top-logo">
                <a href="/admin" class="logo">
                    <img src="{{asset('assets/images/stal.jpg')}}" class="img-fluid" alt="">
                </a>
            </div>
        </div>
        <nav class="navbar navbar-expand-lg navbar-light p-0">
            <div class="navbar-left">
                <div class="iq-search-bar">
                    <div class="searchbox">
                        <div class="dropdown session-dp">
                            <button class="btn dropdown-toggle d-flex justify-content-between align-items-center session-btn" type="button" data-toggle="dropdown">
                                <span> {{App::getLocale()}} </span>
                                <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu p-0">
                                <form action="/admin/language" method="POST" id="kr">
                                    @csrf
                                    <input type="hidden" name="lang" value="kr" >
                                    <li class="{{App::getLocale() == 'kr'?'active':''}}">
                                        <a class="px-4" href="javascript:{}" onclick="document.getElementById('kr').submit();"> Korea </a>
                                    </li>
                                </form>
                                <form action="/admin/language" method="POST" id="en">
                                    @csrf
                                    <input type="hidden" name="lang" value="en" >
                                    <li class="{{App::getLocale() == 'en'?'active':''}}">
                                        <a class="px-4" href="javascript:{}" onclick="document.getElementById('en').submit();"> English </a>
                                    </li>
                                </form>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <ul class="navbar-list ml-auto mr-md-5 mr-xl-5">
                <li>
                    <a href="#" class="search-toggle iq-waves-effect d-flex align-items-center bg-primary rounded">
                        <img src="{{asset('assets/images/user/1.jpg')}}" class="img-fluid rounded mr-3">
                        <div class="caption">
                            <h6 class="mb-0 line-height text-white">{{__('admin.Admin')}}</h6>
                            <span class="font-size-12 text-white">{{__('admin.Available')}}</span>
                        </div>
                    </a>
                    <div class="iq-sub-dropdown iq-user-dropdown">
                        <div class="iq-card shadow-none m-0">
                            <div class="iq-card-body p-0 ">
                                <div class="bg-primary p-3">
                                    <h5 class="mb-0 text-white line-height">{{__('admin.hello')}} {{__('admin.Admin')}}</h5>
                                    <span class="text-white font-size-12">{{__('admin.Available')}}</span>
                                </div>
                                <div class="d-inline-block w-100 text-center p-3">
                                    <form action="/admin/logout" method="post">
                                        @csrf
                                        <button class="bg-primary iq-sign-btn" type="submit"> {{__('admin.sign_out')}}<i class="ri-login-box-line ml-2"></i> </button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
            </ul>
        </nav>


    </div>
</div>
<!-- TOP Nav Bar END -->
