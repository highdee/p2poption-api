<?php

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;


Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::prefix('v1')->group(function(){
//            Affiliate route
    Route::prefix('affiliate')->group(function(){
        Route::post('create-account',"Affiliate\LoginController@createAccount");
        Route::post('login',"Affiliate\LoginController@login");
        Route::post('forget-password',"Affiliate\LoginController@forget_password");
        Route::post('reset-password',"Affiliate\LoginContgetroller@reset_password");
        Route::get('verify-reset-token',"Affiliate\LoginController@verify_password");
        Route::get('increase-visitor', "Affiliate\LoginController@IncreaseVisitor");

//            Affiliate authenticated route
        Route::middleware('jwt.auth')->group(function(){
            Route::post('update-affiliate', 'Affiliate\AffiliateController@updateAffiliate');
            Route::get('get-affiliate-dashboard', 'Affiliate\AffiliateController@getAffiliateDashboard');
            Route::get('get-bonus', 'Affiliate\AffiliateController@getBonus');
            Route::post('create-affiliate-link', 'Affiliate\ReferralController@createAffiliateLink');
            Route::post('delete-affiliate-link', 'Affiliate\ReferralController@deleteAffiliateLink');
            Route::get('get-affiliate-bonus', 'Affiliate\ReferralController@getAffiliateBonus');
        });

    });

//    user auth route
    Route::prefix('')->group(function(){
        Route::post('create-account',"AuthController\LoginController@createAccount");
        Route::post('login',"AuthController\LoginController@login");
        Route::post('forget-password',"AuthController\LoginController@forget_password");
        Route::post('reset-password',"AuthController\LoginController@reset_password");
        Route::get('verify-reset-token',"AuthController\LoginController@verify_password");
        Route::get('verify-account',"AuthController\LoginController@verify_account");
        Route::get('resend-verification',"AuthController\LoginController@resend_verification");
    });

//    Authenticated route
    Route::middleware('jwt.auth')->group(function(){
        Route::get('charge-user-balance/{account}/{trade_amount}',"AuthController\UserController@chargeBalance");

        Route::post('update-profile', "AuthController\UserController@updateProfile");
        Route::post('upload-profile-image','AuthController\UserController@uploadProfileImage');
        Route::get('delete-avatar', 'AuthController\UserController@deleteAvatar');
        Route::post('change-password','AuthController\UserController@changePassword');
        Route::get('resend-verification', 'AuthController\UserController@resend_verification');
        Route::get('get-dashboard', 'AuthController\UserController@get_dashboard');
        Route::post('verify-details', 'AuthController\UserController@verifyDetails');
        Route::get('send-phone-verification',"AuthController\UserController@sendPhoneToken");
        Route::post('send-chat',"AuthController\UserController@sendChat");
        Route::get('get-support-chat',"AuthController\UserController@getSupportChat");
        Route::get('login-history', "AuthController\UserController@loginHistory");
        Route::post('logout',"AuthController\UserController@logout");

        Route::get('credit-affiliate-referral-bonus/{user_id}/{amount}/{action}', 'AuthController\UserController@creditAffiliateReferralBonus');

        Route::get('get-deposit-currency', 'AuthController\UserController@getDepositCurrency');
        Route::post('verify-currency-exist', 'AuthController\UserController@verifyCurrencyExist');
    });

//    TRADING ROUTES
    Route::middleware('jwt.auth')->group(function(){
        Route::post('update-trading-info', "TradingController@updateTradingInfo");
        Route::post('credit-demo-wallet', "TradingController@creditDemoWallet");
        Route::post('wallet-history', "TradingController@walletHistory");
        Route::post('demo-wallet-history', "TradingController@demoWalletHistory");
        Route::get('trading-histories', "TradingController@allTradingHistory");
    });

    Route::post('update-quick-trading-info', "TradingController@quickUpdateTradingInfo");
    Route::get('quick-trading-histories', "TradingController@quickTradingHistory");

    Route::get('trading-symbols', "TradingController@getSymbols");
//    Payment route
    Route::prefix('payment')->middleware('jwt.auth')->group(function(){
        Route::post('/get-wikipay-address',"Payment\PaymentController@getAddress");
    });
    Route::prefix('payment')->group(function(){
        Route::post('/receive-payment-info',"Payment\PaymentController@receivePaymentInfo");
        Route::post('/upload-wallet-address','Payment\PaymentController@addWalletAddress')->middleware('jwt.auth');
        Route::post('/make-withdrawal-request','Payment\PaymentController@makeWithdrawalRequest')->middleware('jwt.auth');
        Route::get('/get-history','Payment\PaymentController@getHistory')->middleware('jwt.auth');
    });


//    GET FILE CONTENT
    Route::name('display-image')->group(function(){
        Route::get('profile-images/{filename}/{width?}/{height?}',function($filename,$width=0,$height=0){
            $img = Image::make(storage_path().'/profile-images/'.$filename);
            if($width == 0 || $height == 0){
                $height=$img->height();
                $width=$img->width();
            }
            return $img->resize($width, $height)->response('jpg');
        });

        Route::get('verification/{filename}/{width?}/{height?}',function($filename,$width=0,$height=0){
            $img = Image::make(storage_path().'/verification/'.$filename);
            if($width == 0 || $height == 0){
                $height=$img->height();
                $width=$img->width();
            }
            return $img->resize($width, $height)->response('jpg');
        });

        Route::get('qrcodes/{filename}',function($filename){
            $path=storage_path().'/qrcodes/'.$filename;
            return response()->download($path);
        });
    });
});
