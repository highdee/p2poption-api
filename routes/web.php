<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;

Auth::routes();

Route::name('login.')->group(function(){
    Route::get('admin-login', 'AdminController\LoginController@showLogin')->name('admin');
    Route::post('admin-login','AdminController\LoginController@login');
});

Route::prefix('admin')->middleware('language')->group(function(){
    Route::get('/','AdminController\AdminController@index');
    Route::get('users', 'AdminController\AdminController@users');
    Route::get('user/{id}', 'AdminController\AdminController@user');
    Route::post('update-user', 'AdminController\AdminController@updateUser');
    Route::post('logout', 'AdminController\AdminController@logout');

    // Route::get('deposit-bonus', 'AdminController\AdminController@depositBonus');
    Route::post('update-deposit-bonus', 'AdminController\AdminController@updateDepositBonus');

    Route::post('/language', 'AdminController\AdminController@language');

    Route::get('verification-request', 'AdminController\AdminController@VerificationRequest');
    Route::post('verify-identity', 'AdminController\AdminController@verifyIdentity');
    Route::post('verify-address', 'AdminController\AdminController@verifyAddress');
    Route::post('reject-identity', 'AdminController\AdminController@rejectIdentity');
    Route::post('reject-address', 'AdminController\AdminController@rejectAddress');

    Route::get('exchange-rates', 'AdminController\PaymentController@exchangeRates');
    // Route::get('exchange-rate/{id}', 'AdminController\PaymentController@exchangeRate');
    // Route::post('update-exchanges-rate', 'AdminController\PaymentController@updateExchangeRate');
    Route::post('credit-user-account', 'AdminController\PaymentController@creditUserAccount');
    Route::post('verify-credit-token', 'AdminController\PaymentController@verifyCreditToken');

//    Chat Route
    Route::prefix('/chats')->group(function(){
        Route::get('', 'AdminController\AdminController@chats');
        Route::get('{id}', 'AdminController\AdminController@chat');
        Route::post('send-message', 'AdminController\AdminController@sendMessage');
    });


    Route::get('deposits', 'AdminController\PaymentController@deposits');
    Route::get('deposit-currencies', 'AdminController\PaymentController@currencies');
    Route::get('deposit-currency/{id}', 'AdminController\PaymentController@currency');
    Route::post('update-deposit-currency', 'AdminController\PaymentController@updateDepositcurrency');
    Route::get('withdrawal', 'AdminController\PaymentController@withdrawal');
    Route::post('withdrawal/action/{action}', 'AdminController\PaymentController@handleWithdrawal');

//      Affiliate route
    Route::prefix('/affiliates')->group(function(){
        Route::get('', 'AdminController\AffiliateController@index');
        Route::get('/affiliate/{id}', 'AdminController\AffiliateController@affiliate');
        Route::post('update-affiliate', 'AdminController\AffiliateController@updateAffiliate');
        Route::post('ban-affiliate', 'AdminController\AffiliateController@banAffiliate');
        Route::post('delete-affiliate', 'AdminController\AffiliateController@deleteAffiliate');
        Route::get('/profit-sharing', 'AdminController\AffiliateController@profitSharing');
        Route::post('add-profit-sharing', 'AdminController\AffiliateController@addProfitSharing');
        Route::post('update-profit-sharing', 'AdminController\AffiliateController@updateProfitSharing');
        Route::post('delete-profit-sharing', 'AdminController\AffiliateController@deleteProfitSharing');

        Route::get('affiliate-percentage', 'AdminController\AffiliateController@affiliatePercentage');
        Route::post('update-affiliate-percentage', 'AdminController\AffiliateController@updateAffiliatePercentage');
    });

    Route::prefix('/assets')->group(function(){
        Route::get('', 'AdminController\AdminController@allAssets');
        Route::get('create-asset/{id}', 'AdminController\AdminController@createAssetPage');
        Route::post('create-assets/{type}', 'AdminController\AdminController@createAssets');
        Route::post('update-asset', 'AdminController\AdminController@updateAssets');
        Route::get('show-asset/{id}', 'AdminController\AdminController@showAsset');
        Route::post('delete-asset', 'AdminController\AdminController@deleteAsset');
        Route::post('toggle-assets-status', 'AdminController\AdminController@toggleAssetStatus');
        Route::post('create-timings', 'AdminController\AdminController@createTiming');
        Route::get('timings', 'AdminController\AdminController@timings');
        Route::get('create-timing', 'AdminController\AdminController@createTimingPage');
        Route::post('toggle-timing', 'AdminController\AdminController@toggleTiming');
    });
});

